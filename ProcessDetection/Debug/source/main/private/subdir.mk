################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../source/main/private/branch_device_info.c \
../source/main/private/branch_main.c \
../source/main/private/branch_main_controller.c \
../source/main/private/branch_state_active.c \
../source/main/private/branch_state_setup.c \
../source/main/private/branch_state_standby.c 

OBJS += \
./source/main/private/branch_device_info.o \
./source/main/private/branch_main.o \
./source/main/private/branch_main_controller.o \
./source/main/private/branch_state_active.o \
./source/main/private/branch_state_setup.o \
./source/main/private/branch_state_standby.o 

C_DEPS += \
./source/main/private/branch_device_info.d \
./source/main/private/branch_main.d \
./source/main/private/branch_main_controller.d \
./source/main/private/branch_state_active.d \
./source/main/private/branch_state_setup.d \
./source/main/private/branch_state_standby.d 


# Each subdirectory must supply rules for building sources it contributes
source/main/private/%.o: ../source/main/private/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"/media/snap/hdd_2/Neha/ProcessDetection/source/video_algos" -I"/media/snap/hdd_2/Neha/ProcessDetection/source/main" -I"/media/snap/hdd_2/Neha/ProcessDetection/source/main/private" -I"/media/snap/hdd_2/Neha/ProcessDetection/source/common/private" -I"/media/snap/hdd_2/Neha/ProcessDetection/source/common" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


