/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_pvt_messages.h
Author       : Sharath
Description  : This file contains defines for Internal/Private Branch messages
*******************************************************************************/

#ifndef _BRANCH_PVT_MESSAGES_H_
#define _BRANCH_PVT_MESSAGES_H_

/******************************* I N C L U D E S ******************************/

#include "branch_context.h"
#include "branch_types.h"

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

#define BRANCH_PVT_MSG_SIZE		        (512)
#define BRANCH_PVT_MSG_HDR_SIZE		    (sizeof(x_branch_pvt_msg_header))
#define BRANCH_PVT_MIN_PAYLOAD_SIZE		(2)
#define BRANCH_PVT_MAX_PAYLOAD_SIZE		(BRANCH_PVT_MSG_SIZE - \
                                         BRANCH_PVT_MSG_HDR_SIZE)

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/* Pvt Message Request, Pvt Response or Event */
typedef enum
{
    E_PVT_MSG_TYPE_NONE,
    E_PVT_MSG_TYPE_REQUEST,
    E_PVT_MSG_TYPE_RESPONSE,
    E_PVT_MSG_TYPE_EVENT,
    E_PVT_MSG_TYPE_MAX
} e_branch_pvt_msg_t;

typedef enum
{
    E_PVT_MSG_NONE               = 0x00,

    /* MESSAGES */
    E_PVT_MSG_START_SETUP        = 0x01,
    E_PVT_MSG_STOP_SETUP,
    E_PVT_MSG_DISCOVER_IP,
    E_PVT_MSG_IP_DEV_LIST,
    E_PVT_MSG_SET_SOURCE,
	E_PVT_MSG_GET_SOURCE,
    E_PVT_MSG_GET_DEVICE_INFO,
	E_PVT_MSG_SET_DEVICE_INFO,
    E_PVT_MSG_START_VIDEO_DETECT,
    E_PVT_MSG_STOP_VIDEO_DETECT,
    E_PVT_MSG_SET_DEVICE_ID,
    E_PVT_MSG_GET_DEVICE_INFO_LIST,
	E_PVT_MSG_SET_DEVICE_INFO_LIST,

    /* EVENTS */
	E_PVT_EVT_BT_CONNECTED = 0x80,
    E_PVT_EVT_BT_DISCONNECTED,
    E_PVT_EVT_SWITCH_UNCONFIGURED,
    E_PVT_EVT_SWITCH_AVAILABLE,
    E_PVT_EVT_SWITCH_NOT_AVAILABLE,
    E_PVT_EVT_SOURCE_CHANGED,
    E_PVT_EVT_SOURCE_IDENTIFIED,
    E_PVT_EVT_VIDEO_DETECTED,
    E_PVT_EVT_MAX
} e_branch_pvt_event;

/* Message Response  */
typedef enum
{
    E_PVT_MSG_RSP_SUCCESS,
    E_PVT_MSG_RSP_CHECKSUM_FAIL,
    E_PVT_MSG_RSP_INVALID_MSG,
    E_PVT_MSG_RSP_BUSY,
    E_PVT_MSG_RSP_NOT_READY,
    E_PVT_MSG_RSP_MAX
} e_branch_pvt_msg_resp;

/***************************** S T R U C T U R E S ****************************/

/***************
   Structures to send-receive messages within Branch-Switch. */

/* Header */
typedef struct
{
    uint32_t        e_pvt_msg_type;  /* e_branch_pvt_msg_t */
    uint32_t        e_pvt_msg_evt;   /* e_branch_pvt_event */
    uint16_t        msg_checksum;    /* checksum for the entire message
                                        except this field */
    uint16_t        payload_size;    /* size of the rest of the payload
                                        (i.e *p_payload) */
    uint32_t        min_payload[BRANCH_PVT_MIN_PAYLOAD_SIZE];
                                     /* could be result, IR code,
                                        response code (e_branch_pvt_msg_resp) */
    e_queue_id      e_source;
    e_queue_id      e_destination;
} x_branch_pvt_msg_header;

/* Message */
typedef struct
{
    x_branch_pvt_msg_header  x_pvt_msg_hdr;
    uint8_t                  payload[BRANCH_PVT_MAX_PAYLOAD_SIZE];	/* NULL if
						payload_size is zero max size is 488 (512 - 24) */
} x_branch_pvt_message; /* maximum size is MESSAGE_SIZE_MAX bytes */


/****************************** F U N C T I O N S *****************************/
#ifdef    __cplusplus
extern "C" {
#endif
/*******************************************************************************
Function  : validate_pvt_msg

Purpose   : Validates the checksum of the given message

Arguments : px_pvt_msg   pointer to the message

Return    : 0 on success, -ve on error
*******************************************************************************/
int8_t validate_pvt_msg (x_branch_pvt_message *px_pvt_msg);

/*******************************************************************************
Function  : create_pvt_msg

Purpose   : Creates a message with the given parameters

Arguments : px_pvt_msg      pointer to the message
            _e_msg_type     message type (request/response/event)
            _e_msg          message id
            _min_payload    minimum payload (like response id, etc)
            _payload_size   payload size
            _puc_data       payload

Return    : 0 on success, -ve on error
*******************************************************************************/
int8_t create_pvt_msg (x_branch_pvt_message *px_pvt_msg,
                                   uint16_t  _e_msg_type,
                                   uint16_t  _e_msg,
                                   uint32_t  _min_payload[],
                                   uint16_t  _payload_size,
                                   uint8_t  *_puc_data,
								   e_queue_id _e_source,
								   e_queue_id _e_destination);

/*******************************************************************************
Function  : calculate_pvt_msg_checksum

Purpose   : calculates the checksum of the given message

Arguments : px_pvt_msg        pointer to the message

Return    : 0 on success, -ve on error
*******************************************************************************/
uint16_t calculate_pvt_msg_checksum (x_branch_pvt_message *px_pvt_msg);

/*******************************************************************************
Function  : init_send_msg

Purpose   : allocates memory for a response message for a queue

Arguments : px_pvt_msg  pointer to the message
            e_q_id      queue where the message has to be created

Return    : 0 on success, -ve on error
*******************************************************************************/
int8_t init_send_msg (x_branch_pvt_message *p_x_br_pvt_msg,
                            e_queue_id e_q_id);

/*******************************************************************************
Function  : get_pvt_msg_size

Purpose   : calculates the size of the given message

Arguments : px_pvt_msg        pointer to the message

Return    : +ve on success, -ve on error
*******************************************************************************/
uint32_t get_pvt_msg_size (x_branch_pvt_message *px_pvt_msg);

#ifdef    __cplusplus
}
#endif

#endif /* #ifndef _BRANCH_PVT_MESSAGES_H_ */

/* EOF branch_pvt_messages.h */
