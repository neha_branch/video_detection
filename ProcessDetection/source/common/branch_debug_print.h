/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
File name   : branch_debug_print.h
Author      : RAHUL NAKHATE
Description :
*******************************************************************************/

#ifndef _BRANCH_DEBUG_PRINT_H
#define _BRANCH_DEBUG_PRINT_H

/******************************* I N C L U D E S ******************************/

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/
typedef enum
{
	E_DEBUG_LEVEL_INFO,
	E_DEBUG_LEVEL_DEBUG,
	E_DEBUG_LEVEL_ERROR,
	E_DEBUG_LEVEL_CRITICAL_ERROR,
	E_DEBUG_LEVEL_SILENT
}e_debug_levels_t;
/****************************** F U N C T I O N S *****************************/
#ifdef    __cplusplus
extern "C" {
#endif
/*******************************************************************************
Function     : debug_printf
Description  : Prints the given message to console
Arguments    : Message level
			   Message
Return value : None
*******************************************************************************/
void  debug_printf (int level, const char *fmt , ...);

#ifdef    __cplusplus
}
#endif
#endif /* _BRANCH_DEBUG_PRINT_H */
