/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_types.h
Author       : Sharath
Description  : This file contains the commonly used types
*******************************************************************************/

#ifndef _BRANCH_TYPES_H
#define _BRANCH_TYPES_H

/******************************* I N C L U D E S ******************************/

#include "branch_defines.h"
#include <stdint.h>
#include <stddef.h>

/******************************* T Y P E D E F S ******************************/
#if !defined(NULL)
    #define NULL 0
#endif
/*
   These are defines for the PC.

   char    8 bits
   short  16 bits
   int    32 bits
   long   64 bits
  */

typedef unsigned char	uint8_t;

typedef unsigned short	uint16_t;

typedef unsigned int	uint32_t;

typedef unsigned long	uint64_t;

//typedef char			int8_t;

typedef short			int16_t;

typedef int				int32_t;

typedef long			int64_t;

typedef unsigned char	bool_t;

//typedef unsigned int    size_t;

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/

/****************************** F U N C T I O N S *****************************/

#endif /* #ifndef _BRANCH_TYPES_H */

/* EOF branch_types.h */
