/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
* File name   : common_header.h
*
* Author      : Siddharth Kumar
*
* Description : Definition of common elements across all sub-modules
*
*******************************************************************************/
#ifndef _COMMON_HEADER_H
#define _COMMON_HEADER_H

/******************************* I N C L U D E S ******************************/
#include "branch_debug_print.h"
//#include "branch_defines.h"

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/
#ifndef MAX_PATH
#define MAX_PATH                (256)
#endif

#define MAX_NAME_LENGTH		(30)
#define NUM_DEVICES			(14 + 1) /* +1 for invalid case */
#define NUM_VERSIONS		(7 + 1)  /*                     */
#define NUM_SCHEMES			(3)

#ifdef VIDEO_RES_VGA
    #define VIDEO_FRAME_WIDTH   (640)
    #define VIDEO_FRAME_HEIGHT  (360)
#else
    #define VIDEO_FRAME_WIDTH   (1920)
    #define VIDEO_FRAME_HEIGHT  (1080)
#endif

#define VIDEO_DETECT_FRAME_WIDTH    (640)
#define VIDEO_DETECT_FRAME_HEIGHT   (360)
/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/
const char DEVICE_NAMES[NUM_DEVICES][MAX_NAME_LENGTH] = 
{
	"NO_DEVICE",
	"ANDROIDTV",
	"APPLETV",
	"CHROMECAST",
	"DIRECTV",
	"FIRETV",
	"PS",
	"ROKU",
	"WD",
	"WII",
	"XBOX",
	"SONYBP",
	"SAMSUNGBP",
	"PANASONICBP",
	"NEXUSPLAYER"
};

const char DEVICE_VER[NUM_VERSIONS][MAX_NAME_LENGTH] = 
{
	"NO_VERSION",
	"ONE",
	"360",
	"1",
	"2",
	"3",
	"4",
	"STICK"
};

/*************************** E N U M E R A T I O N S **************************/
typedef enum eDEVICE_NAMES
{
	NO_DEVICE = 0,
	ANDROIDTV,
	APPLETV,
	CHROMECAST,
	DIRECTV,
	FIRETV,
	PS,
	ROKU,
	WD,
	WII,
	XBOX,
	SONYBP,
	SAMSUNGBP,
	PANASONICBP,
	NEXUSPLAYER,
	MAX_DEVICES
};

typedef enum eDEVICE_VER
{
	NO_VERSION = 0,
	ONE_STR,
	THREESIXTY,
	ONE,
	TWO,
	THREE,
	FOUR,
	STICK,
	MAX_VERSIONS
};

typedef enum eVID_DETECTION_SCHEME
{
	eFINGERPRINTING     = 0x01,
	eLOGO_DETECTION     = 0x02,
	eTEMPLATE_MATCHING  = 0x03,
	eSCHEME_ALL         = 0x07  /* bitwise OR of all the above elements in this enum.
						           UPDATE WHEN NEW ELEMENT IS ADDED */
};

/***************************** S T R U C T U R E S ****************************/
typedef struct deviceIdInfo
{
	int devCategory;
	int devBrand;
	int devModel;
	int devVer;
} deviceIdInfo;

typedef struct detectInfo
{
	int detScheme;
	double measureVal;
	int perfectMatch;
} detectInfo;

typedef struct vidDetectPayload
{
	deviceIdInfo sDeviceIdInfo;
	detectInfo sDetectInfo;
} vidDetectPayload;

/****************************** F U N C T I O N S *****************************/
int get_device_id(char dev_name[MAX_NAME_LENGTH]);
int get_device_version(char dev_ver_string[MAX_NAME_LENGTH]);

#endif /* _COMMON_HEADER_H */
