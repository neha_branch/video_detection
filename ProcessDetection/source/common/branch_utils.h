/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_utils.h
Author       : Sharath
Description  : This file contains declarations for utility functions
*******************************************************************************/

#ifndef _BRANCH_UTILS_H_
#define _BRANCH_UTILS_H_

/******************************* I N C L U D E S ******************************/

#include "branch_types.h"
#include "branch_context.h"

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/


/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/

/****************************** F U N C T I O N S *****************************/

#ifdef    __cplusplus
extern "C" {
#endif
/* Message Queue wrappers */

/*******************************************************************************
Function  : br_create_q

Purpose   : Creates a message queue

Arguments :
            e_queue_id  Module identifier
            max_msgs   Maximum storage to be allocated for messages in queue.
                       MQ will at any time, hold these many messages, before
                       br_send_msg() will block for br_recv_msg()
            max_size   Size of messages in message queue.
            pvt_data_size   Size of private data in message queue.
                       Every message if required can be associated with this.

Return    : returns handle to newly created message queue on success, or NULL
            on error
*******************************************************************************/
void *br_create_q (uint8_t e_queue_id,
                       size_t max_msgs,
                       size_t max_size,
                       size_t pvt_data_size);


/*******************************************************************************
Function  : br_send_msg

Purpose   : Sends a message to a message queue

Arguments :
            e_queue_id Module identifier
            msg       buffer containing message to be sent
            pvt_data  buffer containing private data to be sent
            timeout   timeout for send operation in milliseconds

Return    : returns 0 if the message was successfully sent. otherwise -1 is
            returned
*******************************************************************************/
int br_send_msg (uint8_t e_queue_id, void *msg, void *pvt_data, int timeout);

/*******************************************************************************
Function  : br_recv_msg

Purpose   : Receives a message from a message queue

Arguments :
            e_queue_id Module identifier
            msg       buffer containing message to be received
            pvt_data  buffer containing private data to be received
            timeout   timeout for send operation in milliseconds

Return    : returns 0 if the message was successfully sent. otherwise -1 is
            returned
*******************************************************************************/
int br_recv_msg (uint8_t e_queue_id, void **msg, void **pvt_data, int timeout);

/*******************************************************************************
Function  : br_lock_q

Purpose   : Locks the message queue from usage

Arguments :
            e_queue_id  Module identifier

Return    : returns 0 if the message was successfully sent. otherwise -1 is
            returned
*******************************************************************************/
int br_lock_q (uint8_t e_queue_id);

/*******************************************************************************
Function  : br_reset_q

Purpose   : Resets the message queue (only if it was already locked)

Arguments :
            e_queue_id  Module identifier

Return    : returns 0 if the message was successfully sent. otherwise -1 is
            returned
*******************************************************************************/
int br_reset_q (uint8_t e_queue_id);

/*******************************************************************************
Function  : br_get_q_lock_status

Purpose   : Returns the lock status(locked/un-locked) of the queue.

Arguments :
            e_queue_id  Module identifier

Return    : 1: locked; 0: un-locked; -ve: Error
*******************************************************************************/
int br_get_q_lock_status (uint8_t e_queue_id);

/*******************************************************************************
Function  : to_little_endian

Purpose   : Converts the given big endian data to little endian data

Arguments :
            p_big_endian_data  Pointer to big endian data
			size               Size of data

Return    : returns 0 if the data was successfully converted. otherwise -1 is
            returned
*******************************************************************************/
int32_t to_little_endian (uint8_t *p_big_endian_data, uint8_t size);

/*******************************************************************************
Function  : to_big_endian

Purpose   : Converts the given little endian data to big endian data

Arguments :
            p_little_endian_data  Pointer to little endian data
			size                  Size of data

Return    : returns 0 if the data was successfully converted. otherwise -1 is
            returned
*******************************************************************************/
uint32_t to_big_endian (uint8_t *p_little_endian_data, uint8_t size);

/*******************************************************************************
Function  : reverse_string

Purpose   : Reverses the given string

Arguments :
            p_string  Pointer to string
			size      Size of string

Return    : returns 0 if the data was successfully reversed. otherwise -1 is
            returned
*******************************************************************************/
int32_t reverse_string (uint8_t *p_string, uint8_t size);

/*******************************************************************************
Function  : calculate_checksum

Purpose   : Calculates the checksum of the given data

Arguments :
            p_string  Pointer to data
			size      Size of data

Return    : returns calculated checksum
*******************************************************************************/
uint16_t calculate_checksum (uint8_t *p_buffer, uint16_t size);

/*******************************************************************************
Function  : br_free_msguint16_t calculate_checksum (uint8_t *p_buffer, uint16_t size);

Purpose   : Frees one message

Arguments :
            e_queue_id  Module identifier

Return    : returns 0 if the message was successfully sent. otherwise -1 is
            returned
*******************************************************************************/
int br_free_msg (uint32_t e_queue_id);

#ifdef    __cplusplus
}
#endif


#endif /* #ifndef _BRANCH_UTILS_H_ */

/* EOF branch_utils.h */
