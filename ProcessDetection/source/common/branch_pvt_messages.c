/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
File name   : branch_pvt_messages.c
Author      : Sharath
Description : This file contains the set of functions related to private message
*******************************************************************************/

/******************************* I N C L U D E S ******************************/

#include <string.h>
#include <stddef.h>

#include "branch_types.h"
#include "branch_utils.h"
#include "branch_pvt_messages.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

int8_t validate_pvt_msg (x_branch_pvt_message *px_pvt_msg)
{
    int8_t   ret_val  = 0;

    uint32_t msg_size = 0;
    uint16_t checksum = 0;

    if( NULL == px_pvt_msg )
    {
        ret_val  = -1;
        goto LABEL_RETURN;
    }

    msg_size  = get_pvt_msg_size (px_pvt_msg);
    checksum  = calculate_pvt_msg_checksum (px_pvt_msg);

    /* check if message checksum is valid */
    if( checksum != px_pvt_msg->x_pvt_msg_hdr.msg_checksum )
    {
        /* TODO : Send response (Invalid checksum) */
        ret_val = -2;
        goto LABEL_RETURN;
    }

LABEL_RETURN:
    return ret_val;
}


int8_t create_pvt_msg (x_branch_pvt_message *px_pvt_msg,
                                   uint16_t  _e_msg_type,
                                   uint16_t  _e_msg,
                                   uint32_t  _min_payload[],
                                   uint16_t  _payload_size,
                                   uint8_t  *_puc_data,
								   e_queue_id _e_source,
								   e_queue_id _e_destination)
{
    int8_t ret_val = 0;
    
    if( NULL == px_pvt_msg )
    {
        ret_val = -1;
        goto LABEL_RETURN;
    }

    (void)memset (px_pvt_msg, 0, sizeof(x_branch_pvt_message));

    /* put all data in packet */
    px_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type = _e_msg_type;
    px_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt  = _e_msg;

    if( NULL != _min_payload )
    {
        memcpy (px_pvt_msg->x_pvt_msg_hdr.min_payload, _min_payload,
                    sizeof(_min_payload) * BRANCH_PVT_MIN_PAYLOAD_SIZE);
    }
    px_pvt_msg->x_pvt_msg_hdr.payload_size = _payload_size;
    if( 0 != _payload_size )
    {
        memcpy (px_pvt_msg->payload, _puc_data, _payload_size);
    }
	px_pvt_msg->x_pvt_msg_hdr.e_source = _e_source;
	px_pvt_msg->x_pvt_msg_hdr.e_destination = _e_destination;

    /* Calculate message checksum */
    px_pvt_msg->x_pvt_msg_hdr.msg_checksum =
                calculate_pvt_msg_checksum (px_pvt_msg);

LABEL_RETURN:
    return ret_val;
}

uint16_t
calculate_pvt_msg_checksum (x_branch_pvt_message *px_pvt_msg)
{
    uint16_t msg_checksum = 0;
    uint16_t tmp_checksum = 0;
    uint32_t msg_size     = 0;

    if( NULL == px_pvt_msg )
    {
        goto CLEAN_RETURN;
    }

    msg_size = get_pvt_msg_size (px_pvt_msg);
    if( BRANCH_PVT_MAX_PAYLOAD_SIZE < msg_size )
    {
        goto CLEAN_RETURN;
    }

    /* Get the message checksum */
    tmp_checksum = px_pvt_msg->x_pvt_msg_hdr.msg_checksum;
    /* Put checksum as zero */
    px_pvt_msg->x_pvt_msg_hdr.msg_checksum = 0;

    /* Calculate the checksum */
    msg_checksum = calculate_checksum ((uint8_t*) px_pvt_msg,
                                          msg_size);

    /* Put the original checksum back */
    px_pvt_msg->x_pvt_msg_hdr.msg_checksum = tmp_checksum;

CLEAN_RETURN:
    return (msg_checksum);
}

uint32_t
get_pvt_msg_size (x_branch_pvt_message *px_pvt_msg)
{
    uint32_t msg_size = 0;

    if( NULL == px_pvt_msg )
    {
        goto CLEAN_RETURN;
    }

    msg_size = sizeof (px_pvt_msg->x_pvt_msg_hdr);
    msg_size += px_pvt_msg->x_pvt_msg_hdr.payload_size;

CLEAN_RETURN:
    return (msg_size);
}

int8_t init_send_msg (x_branch_pvt_message *p_x_br_pvt_msg,
                             e_queue_id e_q_id)
{
    x_branch_pvt_message *p_x_resp_pvt_msg = NULL;

    int        ret_val = -1;

    if( (NULL == p_x_br_pvt_msg) || (E_QUEUE_MAX <= e_q_id) )
    {
        goto LABEL_RETURN;
    }

    ret_val = br_get_resp_msg ((void **)&p_x_resp_pvt_msg, e_q_id);
    if( (0 != ret_val) || (NULL == p_x_resp_pvt_msg) )
    {
        /* queue not initialized/created */
        goto LABEL_RETURN;
    }
    
    /* copy message header  */
    (void)memset (p_x_resp_pvt_msg, 0, sizeof(x_branch_pvt_message));

    /* invalidate the checksum */
    p_x_resp_pvt_msg->x_pvt_msg_hdr.msg_checksum = 0;

LABEL_RETURN:
    return (ret_val);
}

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/


/* EOF branch_pvt_messages.c */
