/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
File name   : branch_debug_print.c
Author      : RAHUL NAKHATE
Description :
*******************************************************************************/

/******************************* I N C L U D E S ******************************/
#include <stdarg.h>
#include <stdio.h>

//#include "branch_types.h"

#include "branch_debug_print.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/
static int debug_flag = E_DEBUG_LEVEL_INFO;

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

void
debug_printf (int level, const char *fmt , ...)
{
	char ca_str[2048];

	va_list arg_ptr;
	va_start (arg_ptr, fmt);
	(void) vsprintf (ca_str, fmt, arg_ptr);
	va_end (arg_ptr);

    if( level >= debug_flag )
    {
	    switch( level )
	    {
			case( E_DEBUG_LEVEL_INFO ):
			{
				printf ("\n*** INFO *** | %s", ca_str);
			}
		    break;
			case( E_DEBUG_LEVEL_DEBUG ):
			{
				printf ("\n*** DEBUG *** | %s", ca_str);
			}
		    break;
			case( E_DEBUG_LEVEL_ERROR ):
			{
				printf ("\n*** ERROR *** | %s", ca_str);
			}
		    break;
			case( E_DEBUG_LEVEL_CRITICAL_ERROR ):
			{
				printf ("\n*** CRITICAL ERROR *** | %s", ca_str);
			}
		    break;
			
			default:
			{
				printf( "%s", ca_str );
			}
	    }
    }
    
}  /* debug_print () */


/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/


/* EOF branch_debug_print.c */
