/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_pvt_msg_q.h
Author       : Sharath
Description  : This file contains defines for message queues
*******************************************************************************/

#ifndef _BRANCH_PVT_MESG_Q_H_
#define _BRANCH_PVT_MESG_Q_H_

/******************************* I N C L U D E S ******************************/

#include "branch_types.h"

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/

/** type of message queue handle */
typedef   void    *mp_q_handle;

typedef struct
{
  size_t      max_msgs;      /** Maximum number of messages that can
                                be simultaneously stored on queue */
  size_t      msg_size;      /** Size of messages */
  size_t      pvt_data_size; /** Size of private data element */
  size_t      msg_count;     /** Number of messages  currently in
              queue. Actual value may change before application is
              able to read this value */
} mq_params;

/****************************** F U N C T I O N S *****************************/

/*******************************************************************************
Function  : mq_create_q

Purpose   : Creates a message queue

Arguments :
            max_msgs   Maximum storage to be allocated for messages in queue.
                       MQ will at any time, hold these many messages, before
                       mq_send_msg() will block for mq_recv_msg()
            max_size   Size of messages in message queue.
            pvt_data_size   Size of private data in message queue.
                       Every message if required can be associated with this.

Return    : returns handle to newly created message queue on success, or NULL
            on error
*******************************************************************************/
mp_q_handle mq_create_q (size_t max_msgs, size_t max_size, size_t pvt_data_size);


/*******************************************************************************
Function  : mq_send_msg

Purpose   : Sends a message to a message queue

Arguments :
            mqHandle  Handle to queue created by a call to mq_create_q()
            msg       buffer containing message to be sent
            pvt_data  buffer containing private data to be sent
            timeout   timeout for send operation in milliseconds

Return    : returns 0 if the message was successfully sent. otherwise -1 is
            returned
*******************************************************************************/
int mq_send_msg (mp_q_handle mqHandle, void *msg, void *pvt_data, int timeout);

/*******************************************************************************
Function  : mq_recv_msg

Purpose   : Receives a message from a message queue

Arguments :
            mqHandle  Handle to queue created by a call to mq_create_q()
            msg       buffer containing message to be received
            pvt_data  buffer containing private data to be received
            timeout   timeout for send operation in milliseconds

Return    : returns 0 if the message was successfully sent. otherwise -1 is
            returned
*******************************************************************************/
int mq_recv_msg (mp_q_handle mqHandle,
                 void **msg, void **pvt_data, int timeout);

/*******************************************************************************
Function  : mq_lock

Purpose   : Locks the message queue from usage

Arguments :
            mqHandle  Handle to queue created by a call to mq_create_q()

Return    : returns 0 if the message was successfully sent. otherwise -1 is
            returned
*******************************************************************************/
int mq_lock (mp_q_handle mqHandle);

/*******************************************************************************
Function  : mq_reset

Purpose   : Resets the message queue (only if it was already locked)

Arguments :
            mqHandle  Handle to queue created by a call to mq_create_q()

Return    : returns 0 if the message was successfully sent. otherwise -1 is
            returned
*******************************************************************************/
int mq_reset (mp_q_handle mqHandle);

/*******************************************************************************
Function  : mq_free_msg

Purpose   : Frees one message

Arguments :
            mqHandle  Handle to queue created by a call to mq_create_q()

Return    : returns 0 if the message was successfully sent. otherwise -1 is
            returned
*******************************************************************************/
int mq_free_msg (mp_q_handle mqHandle);

/*******************************************************************************
Function  : mq_get_lock_status

Purpose   : Gets lock status of the queue.

Arguments :
            mqHandle  Handle to queue created by a call to mq_create_q()

Return    : 1: locked; 0: Unlocked; -ve: Error
*******************************************************************************/
int mq_get_lock_status (mp_q_handle mqHandle);

#endif /* #ifndef _BRANCH_PVT_MESG_Q_H_ */

/* EOF branch_pvt_msg_q.h */
