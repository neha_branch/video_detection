#include "common_header.h"
#include <iostream>
#include <string.h>
#include <algorithm>

using namespace std;

int get_device_id(char dev_name[MAX_NAME_LENGTH])
{
    int deviceIndex = 0;/* Initialise with NO_DEVICE */
    int i;
    std::string dev_name_string = string(dev_name);
    std::transform(dev_name_string.begin(), dev_name_string.end(), dev_name_string.begin(), ::tolower);

	for( i = 1; i < NUM_DEVICES; i++)
	{
		string db_device_name = string(DEVICE_NAMES[i]);
		std::transform(db_device_name.begin(), db_device_name.end(), db_device_name.begin(), ::tolower);

		if(strcmp(dev_name_string.c_str(), db_device_name.c_str()) == 0)
		{
			deviceIndex = i;
            break;
		}
	}
    return (deviceIndex);
}

int get_device_version(char dev_ver[MAX_NAME_LENGTH])
{
    int versionIndex = 0;
    int i;
    string dev_ver_string = string(dev_ver);

    std::transform(dev_ver_string.begin(), dev_ver_string.end(), dev_ver_string.begin(), ::tolower);

	if(dev_ver_string[0] != '\0')
	{
		for( i = 1; i < NUM_VERSIONS; i++)
		{
			string db_device_ver = string(DEVICE_VER[i]);
			std::transform(db_device_ver.begin(), db_device_ver.end(), db_device_ver.begin(), ::tolower);

			if(strcmp(dev_ver_string.c_str(), db_device_ver.c_str()) == 0)
			{
				versionIndex = i;
                break;
			}
		}
		dev_ver_string[0] = '\0';
	}
    return (versionIndex);
}
