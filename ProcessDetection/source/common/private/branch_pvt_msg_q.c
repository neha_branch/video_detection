/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_pvt_msg_q.c
Author       : Sharath
Description  : This file contains implementation for message queues
*******************************************************************************/

/******************************* I N C L U D E S ******************************/
#include "branch_pvt_msg_q.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

typedef struct
{
    mq_params       params;
    unsigned char  *buffer;
    unsigned char  *pvt_data;
    size_t          head_index;
    size_t          tail_index;

    unsigned char   q_lock;
    unsigned char   q_clear;

    /* TODO : Add semaphores to make this thread-safe */
} q_context;

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

mp_q_handle mq_create_q (size_t max_msgs, size_t max_size, size_t pvt_data_size)
{
    q_context *context = NULL;

    context = (q_context *)malloc(sizeof(q_context));
    if( NULL == context )
    {
        goto LABEL_RETURN;
    }

    context->params.max_msgs      = max_msgs;
    context->params.msg_size      = max_size;
    context->params.pvt_data_size = pvt_data_size;
    context->params.msg_count     = 0;
    context->head_index           = 0;
    context->tail_index           = 0;
    context->q_lock               = 0;
    context->q_clear              = 0;

    context->buffer               = NULL;
    context->pvt_data             = NULL; 

    context->buffer = (unsigned char *)malloc( max_msgs * max_size );
    if( NULL == context->buffer )
    {
        goto LABEL_RETURN;
    }
    memset( context->buffer, 0, max_msgs * max_size );

    if( 0 != pvt_data_size ) /* optional */
    {
        context->pvt_data = (unsigned char *)malloc( max_msgs * pvt_data_size );
        if( NULL == context->pvt_data )
        {
            goto LABEL_RETURN;
        }
        memset( context->pvt_data, 0, max_msgs * pvt_data_size );
    }

LABEL_RETURN:
    return ((void *)context );
}

int mq_send_msg (mp_q_handle mqHandle, void *msg, void *pvt_data, int timeout)
{
    q_context *msg_q   = NULL;
    int        ret_val = -1;

    unsigned long msg_offset      = 0;
    unsigned long pvt_data_offset = 0;
    unsigned long msg_size        = 0;
    unsigned long pvt_data_size   = 0;

    /* check input parameters */
    if( (NULL == mqHandle) || (NULL == msg) )
    {
        goto LABEL_RETURN;
    }
    msg_q = (q_context *)mqHandle;

    if( 1 == msg_q->q_lock )
    {
        /* Queue is locked, do not send the message */

        if( 1 == msg_q->q_clear )
        {
            /* Clear the flags */
            msg_q->params.msg_count     = 0;
            msg_q->head_index           = 0;
            msg_q->tail_index           = 0;
        }
        ret_val = -2;
        goto LABEL_RETURN;
    }

    if( msg_q->params.msg_count == msg_q->params.max_msgs )
    {
        /* Queue is full */

        ret_val = -3;
        goto LABEL_RETURN;
    }

    msg_size        = msg_q->params.msg_size;
    msg_offset      = msg_q->head_index * msg_size;

    memset( &msg_q->buffer[msg_offset], 0, msg_size );
    memcpy( &msg_q->buffer[msg_offset], (char *)msg, msg_size );
    
    msg_q->params.msg_count++;

    if( NULL != pvt_data ) /* optional */
    {
        pvt_data_size   = msg_q->params.pvt_data_size;
        pvt_data_offset = msg_q->head_index * pvt_data_size;
    
        memset( &msg_q->pvt_data[pvt_data_offset], 0, pvt_data_size );
        memcpy( &msg_q->pvt_data[pvt_data_offset], pvt_data, pvt_data_size );
    }

    if( msg_q->head_index < (msg_q->params.max_msgs - 1) )
    {
        msg_q->head_index++;
    }
    else
    {
        msg_q->head_index = 0;
    }

    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}

int mq_recv_msg (mp_q_handle mqHandle, void **msg, void **pvt_data, int timeout)
{
    q_context *msg_q   = NULL;
    int        ret_val = -1;

    unsigned long msg_offset      = 0;
    unsigned long pvt_data_offset = 0;
    unsigned long msg_size        = 0;
    unsigned long pvt_data_size   = 0;

    /* check input parameters */
    if( (NULL == mqHandle) || (NULL == msg) )
    {
        goto LABEL_RETURN;
    }
    msg_q = (q_context *)mqHandle;
/*Overflow*/
    if( (msg_q->head_index == msg_q->tail_index) && (msg_q->params.msg_count != 0 ) )
    {
    	printf("overflow");
    }
    if( 1 == msg_q->q_lock )
    {
        /* Queue is locked, do not receive the message */

        if( 0 == msg_q->q_clear )
        {
            /* Set the Clear flags */
            msg_q->q_clear = 1;
        }
        ret_val = -2;
        goto LABEL_RETURN;
    }

    if( 0 == msg_q->params.msg_count )
    {
        /* Queue is empty */
        ret_val = -3;
        goto LABEL_RETURN;
    }

    msg_size        = msg_q->params.msg_size;
    msg_offset      = msg_q->tail_index * msg_size;

    *msg = (msg_q->buffer + msg_offset);

    if( NULL != pvt_data ) /* optional */
    {
        pvt_data_size   = msg_q->params.pvt_data_size;
        pvt_data_offset = msg_q->tail_index * pvt_data_size;

        *pvt_data = (msg_q->pvt_data + pvt_data_offset);
    }

    if( msg_q->tail_index < (msg_q->params.max_msgs - 1) )
    {
        msg_q->tail_index++;
    }
    else
    {
        msg_q->tail_index = 0;
    }

    /* Message is not freed.
       Has to be explicitly done by the caller who holds the message after
       using it. */
    
    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}

int mq_free_msg (mp_q_handle mqHandle)
{
    q_context *msg_q   = NULL;
    int        ret_val = -1;

    /* check input parameters */
    if( NULL == mqHandle )
    {
        goto LABEL_RETURN;
    }
    msg_q = (q_context *)mqHandle;

    if( 0 == msg_q->params.msg_count )
    {
        /* Should never happen */
        ret_val = -2;
        goto LABEL_RETURN;
    }
    msg_q->params.msg_count--;

    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}

int mq_lock (mp_q_handle mqHandle)
{
    q_context *msg_q   = NULL;
    int        ret_val = -1;

    /* check input parameters */
    if( NULL == mqHandle )
    {
        goto LABEL_RETURN;
    }
    msg_q = (q_context *)mqHandle;

    msg_q->q_lock  = 1;

    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}

int mq_reset (mp_q_handle mqHandle)
{
    q_context *msg_q   = NULL;
    int        ret_val = -1;

    /* check input parameters */
    if( NULL == mqHandle )
    {
        goto LABEL_RETURN;
    }
    msg_q = (q_context *)mqHandle;

    if( 1 == msg_q->q_lock )
    {
        msg_q->params.msg_count = 0;
        msg_q->head_index       = 0;
        msg_q->tail_index       = 0;

        msg_q->q_clear = 0;
        msg_q->q_lock  = 0;
    }

    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}

int mq_get_lock_status (mp_q_handle mqHandle)
{
    q_context *msg_q   = NULL;
    int        ret_val = -1;

    /* check input parameters */
    if( NULL == mqHandle )
    {
        goto LABEL_RETURN;
    }
    msg_q = (q_context *)mqHandle;

    ret_val = msg_q->q_lock;

LABEL_RETURN:
    return (ret_val);
}
/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/
