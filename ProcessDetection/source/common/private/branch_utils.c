/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_utils.c
Author       : Sharath
Description  : This file contains implementation for utility functions
*******************************************************************************/

/******************************* I N C L U D E S ******************************/

#include "branch_pvt_msg_q.h"
#include "branch_utils.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/


/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

void *br_create_q (uint8_t e_queue_id,
                       size_t max_msgs,
                       size_t max_size,
                       size_t pvt_data_size)
{
    x_switch_context *p_x_switch_context = NULL;

    mp_q_handle   handle  = NULL;
    int         ret_val = -1;

    /* check input parameters */
    if( E_QUEUE_MAX <= e_queue_id )
    {
        goto LABEL_RETURN;
    }

    /* get context */
    (void)br_get_context (&p_x_switch_context);
    if( NULL == p_x_switch_context )
    {
        ret_val = -2;
        goto LABEL_RETURN;
    }

    /* make sure Queue is not already created */
    if( NULL != p_x_switch_context->p_q_handle[e_queue_id] )
    {
        ret_val = -3;
        goto LABEL_RETURN;
    }

    handle = mq_create_q (max_msgs, max_size, pvt_data_size);
    if( NULL == handle )
    {
        ret_val = -4;
        goto LABEL_RETURN;
    }

    /* save message handle */
    p_x_switch_context->p_q_handle[e_queue_id] = (void *)handle;

LABEL_RETURN:
    return ((void *)handle );
}

int br_send_msg (uint8_t e_queue_id, void *msg, void *pvt_data, int timeout)
{
    x_switch_context *p_x_switch_context = NULL;

    int         ret_val = -1;

    /* check input parameters */
    if( (E_QUEUE_MAX <= e_queue_id) || (NULL == msg) )
    {
        goto LABEL_RETURN;
    }

    /* get context */
    (void)br_get_context (&p_x_switch_context);
    if( NULL == p_x_switch_context )
    {
        ret_val = -2;
        goto LABEL_RETURN;
    }

    /* make sure Queue is created */
    if( NULL == p_x_switch_context->p_q_handle[e_queue_id] )
    {
        ret_val = -3;
        goto LABEL_RETURN;
    }

    ret_val = mq_send_msg (
                p_x_switch_context->p_q_handle[e_queue_id],
                msg, pvt_data, timeout);

LABEL_RETURN:
    return (ret_val);
}

int br_recv_msg (uint8_t e_queue_id, void **msg, void **pvt_data, int timeout)
{
    x_switch_context *p_x_switch_context = NULL;

    int         ret_val = -1;

    /* check input parameters */
    if( (E_QUEUE_MAX <= e_queue_id) || (NULL == msg) )
    {
        goto LABEL_RETURN;
    }

    /* get context */
    (void)br_get_context (&p_x_switch_context);
    if( NULL == p_x_switch_context )
    {
        ret_val = -2;
        goto LABEL_RETURN;
    }

    /* make sure Queue is created */
    if( NULL == p_x_switch_context->p_q_handle[e_queue_id] )
    {
        ret_val = -3;
        goto LABEL_RETURN;
    }

    ret_val = mq_recv_msg (
                p_x_switch_context->p_q_handle[e_queue_id],
                msg, pvt_data, timeout);

LABEL_RETURN:
    return (ret_val);
}

int br_free_msg (uint32_t e_queue_id)
{
    x_switch_context *p_x_switch_context = NULL;

    int         ret_val = -1;

    /* check input parameters */
    if( E_QUEUE_MAX <= e_queue_id )
    {
        goto LABEL_RETURN;
    }

    /* get context */
    (void)br_get_context (&p_x_switch_context);
    if( NULL == p_x_switch_context )
    {
        ret_val = -2;
        goto LABEL_RETURN;
    }

    /* make sure Queue is created */
    if( NULL == p_x_switch_context->p_q_handle[e_queue_id] )
    {
        ret_val = -3;
        goto LABEL_RETURN;
    }

    ret_val = mq_free_msg (
                p_x_switch_context->p_q_handle[e_queue_id]);

LABEL_RETURN:
    return (ret_val);
}

int br_lock_q (uint8_t e_queue_id)
{
    x_switch_context *p_x_switch_context = NULL;

    int         ret_val = -1;

    /* check input parameters */
    if( E_QUEUE_MAX <= e_queue_id )
    {
        goto LABEL_RETURN;
    }

    /* get context */
    (void)br_get_context (&p_x_switch_context);
    if( NULL == p_x_switch_context )
    {
        ret_val = -2;
        goto LABEL_RETURN;
    }

    /* make sure Queue is created */
    if( NULL == p_x_switch_context->p_q_handle[e_queue_id] )
    {
        ret_val = -3;
        goto LABEL_RETURN;
    }

    ret_val = mq_lock (
                p_x_switch_context->p_q_handle[e_queue_id]);

LABEL_RETURN:
    return (ret_val);
}

int br_reset_q (uint8_t e_queue_id)
{
    x_switch_context *p_x_switch_context = NULL;

    int         ret_val = -1;

    /* check input parameters */
    if( E_QUEUE_MAX <= e_queue_id )
    {
        goto LABEL_RETURN;
    }

    /* get context */
    (void)br_get_context (&p_x_switch_context);
    if( NULL == p_x_switch_context )
    {
        ret_val = -2;
        goto LABEL_RETURN;
    }

    /* make sure Queue is created */
    if( NULL == p_x_switch_context->p_q_handle[e_queue_id] )
    {
        ret_val = -3;
        goto LABEL_RETURN;
    }

    ret_val = mq_reset (
                p_x_switch_context->p_q_handle[e_queue_id]);

LABEL_RETURN:
    return (ret_val);
}

int br_get_q_lock_status (uint8_t e_queue_id)
{
    x_switch_context *p_x_switch_context = NULL;
    int         ret_val = -1;

    /* check input parameters */
    if( E_QUEUE_MAX <= e_queue_id )
    {
        goto LABEL_RETURN;
    }

    /* get context */
    (void)br_get_context (&p_x_switch_context);
    if( NULL == p_x_switch_context )
    {
        ret_val = -2;
        goto LABEL_RETURN;
    }

    /* make sure Queue is created */
    if( NULL == p_x_switch_context->p_q_handle[e_queue_id] )
    {
        ret_val = -3;
        goto LABEL_RETURN;
    }

    ret_val = mq_get_lock_status (
                p_x_switch_context->p_q_handle[e_queue_id]);

LABEL_RETURN:
    return (ret_val);
}

int32_t
to_little_endian (uint8_t *p_big_endian_data, uint8_t size)
{
	int32_t ret_val = -1;

	if ((NULL == p_big_endian_data) ||
		(0    == size))
	{
		goto LABEL_CLEAN_RETURN;
	}
	
	ret_val = reverse_string (p_big_endian_data, size);
	
LABEL_CLEAN_RETURN:
	return (ret_val);
}  /* to_little_endian () */

uint32_t
to_big_endian (uint8_t *p_little_endian_data, uint8_t size)
{
	int32_t ret_val = -1;

	if ((NULL == p_little_endian_data) ||
		(0    == size))
	{
		goto LABEL_CLEAN_RETURN;
	}
	
	ret_val = reverse_string (p_little_endian_data, size);
	
LABEL_CLEAN_RETURN:
	return (ret_val);
}  /* to_big_endian () */

int32_t
reverse_string (uint8_t *p_string, uint8_t size)
{
	int32_t ret_val = -1;
	uint8_t i = 0;
	uint8_t j = 0;

	if ((NULL == p_string) ||
		(0    == size))
	{
		goto LABEL_CLEAN_RETURN;
	}

	i = 0;
	j = size - 1;
	while (i < j)
	{
		uint8_t temp = 0;

		temp	    = p_string[i];
		p_string[i] = p_string[j];
		p_string[j] = temp;

		i++;
		j--;
	}
	
	ret_val = 0;
LABEL_CLEAN_RETURN:
	return (ret_val);
}  /* reverse_string () */

uint16_t
calculate_checksum (uint8_t *p_buffer, uint16_t size)
{
	uint16_t us_checksum = 0;
	uint16_t i = 0;

	if (NULL == p_buffer)
	{
		goto LABEL_CLEAN_RETURN;
	}

	for (i = 0; i < size; i++)
	{
		us_checksum += (uint16_t) p_buffer[i];
	}
	
LABEL_CLEAN_RETURN:
	return (us_checksum);
}  /* calculate_checksum () */

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/
