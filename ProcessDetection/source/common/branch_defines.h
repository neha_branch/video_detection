/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_defines.h
Author       : Sharath
Description  : This file contains system wide defines
*******************************************************************************/

#ifndef _BRANCH_DEFINES_H
#define _BRANCH_DEFINES_H

/******************************* I N C L U D E S ******************************/

/******************************* T Y P E D E F S ******************************/

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/
#define NO_ERROR				 (0)
#ifndef MAX_PATH
#define MAX_PATH				(256)
#endif

#define THREAD_SLEEP_HIGH        (100)
#define THREAD_SLEEP_MED          (10)
#define THREAD_SLEEP_LOW           (1)

#define QUEUE_SIZE_LARGE          (25)
#define QUEUE_SIZE_MED            (10)
#define QUEUE_SIZE_SMALL           (5)

#define MESSAGE_SIZE_MAX         (512)

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/

/****************************** F U N C T I O N S *****************************/

#endif /* #ifndef _BRANCH_DEFINES_H */

/* EOF branch_defines.h */
