/***************************** C O P Y R I G H T *******************************
*               Copyright (C) 2015. id8 Group Branch Studios Inc.              *
*                             All Rights Reserved.                             *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : source.c
Author       : Sharath
Description  : This file contains implementation for message queues
*******************************************************************************/

/******************************* I N C L U D E S ******************************/
#include <string.h>
#include <iostream>

#include "access_dB.h"
#include "common_header.h"

#include "LogoDetection.h"

using namespace std;
typedef unsigned long long int ulong64;

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/
int g_proc_db_idx[NUM_SCHEMES+1] = {0};
int g_array_size[NUM_SCHEMES+1] = {0};
int g_array_idx[NUM_SCHEMES+1] = {0};
json_object* json_res = NULL;

/********************* S T A T I C   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

int get_device_id(string dev_name);
int query_proc_db(int scheme, int *dev_id, json_object** json_result);
int search_for_match(int scheme, int *dev_id, void *data);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/
void reset_db_access_pointer( int scheme )
{
    g_proc_db_idx[scheme] = 0;
    json_res = NULL;
    g_array_size[scheme] = 0;

}
int get_device_id(string dev_name)
{
    json_object  *json       = NULL;
    json_object  *tmp_json   = NULL;
    E_JSON_ERR e_json_err = json_tokener_success;

    x_httpc_params x_params;
    x_httpc_data   x_data;

    string url;

    int dev_id = -1, array_len = 0;
    int ret_val = 0;

    (void)memset (&x_params, 0, sizeof(x_httpc_params));
    (void)memset (&x_data, 0, sizeof(x_httpc_data));

    printf("\n %s: \n",dev_name.c_str());
    url = "http://192.168.6.2:3000/device_detection/device/name/" + dev_name;
    x_params.url = (char*)url.c_str();//"http://192.168.6.46:3000/device_detection/device/name/";

    //strcat (x_params.url, dev_name.c_str());

    /* create json object for post */
    json = json_object_new_object();
    if ( NULL == json )
    {
        fprintf(stderr, "ERROR: Failed to create json handle");
        ret_val = -1;
        goto LABEL_RETURN;
    }

    json_object_object_add (json, "security_code", json_object_new_string("015447b246de176f00035b79824cf547"));
    x_data.p_in_data = (char *)json;
    http_get  (&x_params, &x_data);


    /* free json object */
    json_object_put (json);
    json = NULL;

    if( NULL != x_data.p_out_data )
    {
        //printf("CURL Returned: %d \n%s\n",(int)x_data.out_size, x_data.p_out_data);

        json = json_tokener_parse_verbose (x_data.p_out_data, &e_json_err);
        free (x_data.p_out_data);
    }
    else
    {
        fprintf(stderr, "ERROR: Failed to populate payload");

        ret_val = -2;
        goto LABEL_RETURN;
    }
    if( x_data.out_size < 2 )
    {
        ret_val = -3;
        goto LABEL_RETURN;
    }

    /* TODO
     * json_object_array_length
     * for length
     *  json_object_array_get_idx
     *  for every id
     *      json_object_object_get
     */
    array_len = json_object_array_length(json);

    //printf("length of array list:%d", array_len);
    tmp_json = json_object_array_get_idx(json, 0);
    tmp_json = json_object_object_get(tmp_json, "id");
    //printf("device_id: %d\n", json_object_get_int(tmp_json));

    LABEL_RETURN:
    if(0 == ret_val)
    {
        dev_id = json_object_get_int(tmp_json);
    }
    else
    {
        dev_id = -1;
    }
        return (dev_id);
}

int get_data_from_file(int *dev_id, void* data)
{
    FILE *fptr = NULL;
    char filename[100];
    s_db_ld_data* db_entry;
    int ret_val = 0;

    *dev_id = g_proc_db_idx[eLOGO_DETECTION];
    db_entry = (s_db_ld_data*)data;

    sprintf( filename, "./created/%d.txt",g_proc_db_idx[eLOGO_DETECTION]);
    fptr = fopen( filename, "rb");
    if( NULL == fptr )
    {
        ret_val = 1;
        g_proc_db_idx[eLOGO_DETECTION] = 0;
        goto LABEL_RETURN;
    }
    fread ((void*)&db_entry->num_kps, sizeof(int),1, fptr);

    db_entry->kp_add = (Point*)malloc( db_entry->num_kps * sizeof(Point));
    db_entry->desc_add = (unsigned char*)malloc( db_entry->num_kps * 32 * sizeof(char));

    fread((void*)db_entry->kp_add, db_entry->num_kps * sizeof(Point),1, fptr);
    fread((void*)db_entry->desc_add, db_entry->num_kps * sizeof(char)* 32,1, fptr);

    fclose( fptr );
    g_proc_db_idx[eLOGO_DETECTION]++;

    LABEL_RETURN:
    return ( ret_val );
}

int get_next_db_entry( int scheme, int *dev_id, json_object **json_obj)
{
    json_object* json = NULL; //*json_obj;
    int db_size = 0, ret_val = 0;

    if( (g_array_idx[scheme] < g_array_size[scheme]) && (NULL != json_res))
    {
        json = json_object_array_get_idx(json_res, g_array_idx[scheme]);
        json = json_object_object_get(json, "data");

        //*dev_id = g_proc_db_idx[scheme] - 1 + g_array_idx[scheme];
        //readdata_from_json_obj(json, scheme, data);
        g_array_idx[scheme]++;
    }
    else
    {
        /*call http query*/
        ret_val = query_proc_db(scheme, dev_id, &json);
        if( 1 == ret_val )
        {
            /*TODO*/
            /*assuming end of database*/
            g_proc_db_idx[scheme] = 0;
            *dev_id = 0;
            //ret_val = query_proc_db(scheme, dev_id, &json_res);
            goto LABEL_RETURN;
        }
        json_res = json_object_object_get(json, "data");
        g_array_size[scheme] = json_object_array_length(json_res);
        g_array_idx[scheme] = 0;
        if( 0 < g_array_size[scheme] )
        {
            json = json_object_array_get_idx(json_res, g_array_idx[scheme]);
            json = json_object_object_get(json, "data");
            //readdata_from_json_obj(json, scheme, data);

            g_array_idx[scheme]++;
        }
    }
    if( NULL != json )
    {
        *json_obj = json;
    }
    LABEL_RETURN:

    return (ret_val);
}
int search_for_match(int scheme, int *dev_id, void *data)
{
    int ret_val = 0, query_ind = g_proc_db_idx[scheme];
    int array_len = 0;
    json_object  *json       = NULL;
    json_object  *json_result  = NULL;

    E_JSON_ERR e_json_err = json_tokener_success;

    x_httpc_params x_params;
    x_httpc_data   x_data;
    ulong64 num;

    string fetch_data, fetch_metadata;
    (void)memset (&x_params, 0, sizeof(x_httpc_params));
    (void)memset (&x_data, 0, sizeof(x_httpc_data));

    x_params.url = "http://192.168.6.2:3000/device_detection/db/info";

    /* create json object for post */
    json = json_object_new_object();
    if ( NULL == json )
    {
        fprintf(stderr, "ERROR: Failed to create json handle");
        ret_val = -1;
        goto LABEL_RETURN;
    }

    json_object_object_add (json, "security_code", json_object_new_string("015447b246de176f00035b79824cf547"));
    json_object_object_add (json, "db",   json_object_new_string("processed"));
    json_object_object_add (json, "scheme",    json_object_new_string(SCHEME_NAMES[scheme]));

    memcpy( (void*)&num, (void*)data, sizeof(ulong64));
    json_object_object_add (json, "pattern", json_object_new_int64(num));

    x_data.p_in_data = (char *)json;
    (void)http_get  (&x_params, &x_data);

    json_object_put (json);
    json = NULL;

    if( NULL != x_data.p_out_data )
    {
        //printf("CURL Returned: \n%s\n", x_data.p_out_data);

        //json_result = json_tokener_parse_verbose (x_data.p_out_data, &e_json_err);
		if( 0 == strcmp(x_data.p_out_data, "null"))
		{
			*dev_id = -1;
			ret_val = -2;
		}
		else
		{
			*dev_id = atoi(x_data.p_out_data);
		}
    }
    LABEL_RETURN:
    return (ret_val);
}
int query_proc_db(int scheme, int *dev_id, json_object** jres)
{
    int ret_val = 0, query_ind = g_proc_db_idx[scheme];
    int array_len = 0;
    json_object  *json       = NULL;
    json_object  *qry_array  = NULL;
    json_object  *tmp_json   = NULL;

    json_object *json_result = *jres;
    E_JSON_ERR e_json_err = json_tokener_success;

    x_httpc_params x_params;
    x_httpc_data   x_data;
    int num_elements = 0;

    string fetch_data, fetch_metadata;
    (void)memset (&x_params, 0, sizeof(x_httpc_params));
    (void)memset (&x_data, 0, sizeof(x_httpc_data));

    x_params.url = "http://192.168.6.2:3000/device_detection/db/info";

    /* create json object for post */
    json = json_object_new_object();
    if ( NULL == json )
    {
        fprintf(stderr, "ERROR: Failed to create json handle");
        ret_val = -1;
        goto LABEL_RETURN;
    }
    json_result = json_object_new_object();
        if ( NULL == json_result )
        {
            fprintf(stderr, "ERROR: Failed to create json handle");
            ret_val = -1;
            goto LABEL_RETURN;
        }

    json_object_object_add (json, "security_code", json_object_new_string("015447b246de176f00035b79824cf547"));
    json_object_object_add (json, "db",   json_object_new_string("processed"));
    json_object_object_add (json, "scheme",    json_object_new_string(SCHEME_NAMES[scheme]));

    json_object_object_add (json, "page",    json_object_new_int(query_ind));

    x_data.p_in_data = (char *)json;
    (void)http_get  (&x_params, &x_data);

    json_object_put (json);
    json = NULL;

    if( NULL != x_data.p_out_data )
    {
        //printf("CURL Returned: \n%s\n", x_data.p_out_data);

        json_result = json_tokener_parse_verbose (x_data.p_out_data, &e_json_err);
        free (x_data.p_out_data);
    }
    else
    {
        fprintf(stderr, "ERROR: Failed to populate payload");

        ret_val = -2;
        goto LABEL_RETURN;
    }
    if( NULL != json_result)
    {
        *jres = json_result;
    }
    tmp_json = json_object_object_get(json_result, "device_id");
    *dev_id  = json_object_get_int(tmp_json);

    tmp_json = json_object_object_get(json_result, "data");
    //num_elements = json_object_array_length(tmp_json);

    if( NULL == tmp_json )
    {
        g_proc_db_idx[scheme] = 0;
        ret_val = 1;
    }

    /*if( eFINGERPRINTING != scheme )
    {
        tmp_json = json_object_object_get(json, "data");
        num_elements = json_object_array_length(tmp_json);
        if( 0 < num_elements )
        {
            for (int i = 0; i < num_elements; i++)
            {
                json = json_object_array_get_idx(tmp_json, i);
                json = json_object_object_get(json, "data");
                //readdata_from_json_obj(json, scheme, data);
            }
        }
    }*/

    LABEL_RETURN:
    if (0 == ret_val)
    {
        g_proc_db_idx[scheme]++;
        //printf("\ndata:%s, \n",(char*)data);
    }
    return (ret_val);
}


/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/
