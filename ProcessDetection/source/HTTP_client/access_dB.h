/***************************** C O P Y R I G H T *******************************
*               Copyright (C) 2015. id8 Group Branch Studios Inc.              *
*                            All Rights Reserved.                              *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
File name   : access_dB.h
Author      : Name
Description : This file contains interface(s) for private message handling in 
			  video detection thread.
*******************************************************************************/
#ifndef _ACCESS_DB_H
#define _ACCESS_DB_H

/******************************* I N C L U D E S ******************************/
#include "httpc.h"

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/
#define MAX_NAME_LENGTH     (30)

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/
const char SCHEME_NAMES[4][MAX_NAME_LENGTH] =
{
    "",
    "fingerprint",
    "logo",
    "template"
};
/*typedef enum eVID_DETECTION_SCHEME
{
    eFINGERPRINTING = 0x01,
    eLOGO_DETECTION = 0x02,
    eTEMPLATE_MATCHING = 0x03,
    eSCHEME_ALL = 0x07
};*/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/

/****************************** F U N C T I O N S *****************************/

int get_next_db_entry( int scheme, int *dev_id, json_object **json);

int search_for_match(int scheme, int *dev_id, void *data);

void reset_db_access_pointer( int scheme );

int get_data_from_file(int *dev_id, void* data);

#endif /* _ACCESS_DB_H */
