/***************************** C O P Y R I G H T *******************************
*              Copyright (C) 2015. id8 Group Branch Studios Inc.               *
*                            All Rights Reserved.                              *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : httpc.c
Author       : Sharath
Description  : HTTP client wrapper
*******************************************************************************/

/******************************* I N C L U D E S ******************************/

#include "httpc.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <json-c/json.h>
#include <curl/curl.h>

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

#define CURL_OPT_TIMEOUT     (5)
#define CURL_OPT_FOLLOW      (1)
#define CURL_OPT_REDIR       (1)

#define HTTP_POST_STRING     "POST"
#define HTTP_GET_STRING      "GET"

#define HTTP_ACCEPT_STRING       "Accept: application/json"
#define HTTP_CONTENT_TYPE_STRING "Content-Type: application/json"

/******************************* T Y P E D E F S ******************************/

typedef struct curl_slist X_CURL_LIST;

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

static size_t curl_cb (void   *p_out,
                       size_t  size,
                       size_t  n,
                       void   *p_pvt);

static CURLcode curl_request (CURL         *p_curl,
                              const char   *p_url,
                              x_httpc_data *p_x_data);

static int8_t http_init (x_httpc_params *p_x_params,
                         x_httpc_data   *p_x_data);

static int8_t http_request (x_httpc_params *p_x_params,
                            x_httpc_data   *p_x_data,
                            char           *p_request,
                            char           *p_data);

static X_CURL_LIST *http_headers_init (CURL *p_curl);

/********************* S T A T I C   V A R I A B L E S ************************/

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

int8_t http_get (x_httpc_params *p_x_params, x_httpc_data *p_x_data)
{
    int ret_val  = -1;

    char *p_data = NULL;

    if( (NULL == p_x_params) || (NULL == p_x_data) )
    {
        goto LABEL_RETURN;
    }

    p_data = (char *)json_object_to_json_string(
                                       (json_object *)p_x_data->p_in_data);

    ret_val = http_request (p_x_params, p_x_data, HTTP_GET_STRING, p_data);

LABEL_RETURN:
    return ret_val;
}

int8_t http_post (x_httpc_params *p_x_params, x_httpc_data *p_x_data)
{
    int ret_val  = -1;

    char *p_data = NULL;

    if( (NULL == p_x_params) || (NULL == p_x_data) )
    {
        goto LABEL_RETURN;
    }

    p_data = (char *)json_object_to_json_string(
                                        (json_object *)p_x_data->p_in_data);

    ret_val = http_request (p_x_params, p_x_data, HTTP_POST_STRING, p_data);

LABEL_RETURN:
    return ret_val;
}

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/

static int8_t http_request (x_httpc_params *p_x_params,
                            x_httpc_data   *p_x_data,
                            char           *p_request,
                            char           *p_data)
{
    int ret_val = -1;

    CURLcode curl_result = CURLE_FAILED_INIT;

    CURL         *p_curl = NULL;

    long         resp = 0;
    if( (NULL == p_x_params) || (NULL == p_x_data) )
    {
        goto LABEL_RETURN;
    }

    p_curl = curl_easy_init();
    if ( NULL == p_curl )
    {
        fprintf(stderr, "ERROR: Failed to create curl handle");
        ret_val = -2;
        goto LABEL_RETURN;
    }

    curl_easy_setopt(p_curl, CURLOPT_CUSTOMREQUEST, p_request);
    if ( NULL != p_data )
    {
        curl_easy_setopt(p_curl, CURLOPT_POSTFIELDS, p_data);
    }

    /* fetch page and capture return code */
    curl_result = curl_request (p_curl, p_x_params->url, p_x_data);

    /* check return code */
    if( (curl_result != CURLE_OK) || (p_x_data->out_size < 1) )
    {
        /* log error */
        fprintf(stderr, "ERROR: Failed to fetch url (%s) - curl said: %s",
            p_x_params->url, curl_easy_strerror(curl_result));

        ret_val = -3;
        goto LABEL_RETURN;
    }

    curl_result = curl_easy_getinfo (p_curl, CURLINFO_HTTP_CONNECTCODE, &resp);
    //fprintf(stderr, "HTTP response %u\n", resp);

    int get = 0;
    curl_result = curl_easy_getinfo(p_curl, CURLINFO_RESPONSE_CODE, &get );

    if ( 200 != get )
    {
        fprintf( stderr, "HTTP request response %d ",get);
        ret_val = -4;
        goto LABEL_RETURN;
    }

    ret_val = 0;

LABEL_RETURN:
    return ret_val;
}

static size_t curl_cb (void   *p_out,
                       size_t  size,
                       size_t  n,
                       void   *p_pvt)
{
    int ret_val = -1;

    size_t total_size = size * n;

    x_httpc_data *p_data = NULL;

    //fprintf(stderr, "curl_cb() received %d bytes\n", (int)total_size);

    if( (NULL == p_pvt) || (NULL == p_out) )
    {
        goto LABEL_RETURN;
    }

    p_data = (x_httpc_data *)p_pvt;

    if( 0 < total_size )
    {
        if( NULL ==  p_data->p_out_data )
        {
            p_data->p_out_data = (char *) malloc (total_size + 1);
        }
        else
        {
            /* reallocate to handle subsequent calls */
            p_data->p_out_data = (char *) realloc (
                    p_data->p_out_data, p_data->out_size + total_size + 1);
        }

        if( NULL == p_data->p_out_data )
        {
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }

    /* copy data */
    memcpy (&(p_data->p_out_data[p_data->out_size]), p_out, total_size);
    p_data->out_size += total_size;
    p_data->p_out_data[p_data->out_size] = 0;

    ret_val = total_size;

LABEL_RETURN:
    return ret_val;
}

static CURLcode curl_request (CURL         *p_curl,
                              const char   *p_url,
                              x_httpc_data *p_x_data)
{
    /* http://linux.die.net/man/3/libcurl-errors */
    CURLcode curl_result = CURLE_FAILED_INIT;
    int get = 0;

    X_CURL_LIST *headers = NULL;

    if( (NULL == p_curl) || (NULL == p_url) || (NULL == p_x_data) )
    {
        goto LABEL_RETURN;
    }

    /* init */
    p_x_data->out_size   = 0;
    p_x_data->p_out_data = NULL;

    /* set options */
    curl_easy_setopt(p_curl, CURLOPT_URL, p_url);
    curl_easy_setopt(p_curl, CURLOPT_WRITEFUNCTION, curl_cb);
    curl_easy_setopt(p_curl, CURLOPT_WRITEDATA, (void *)p_x_data);
    curl_easy_setopt(p_curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
    curl_easy_setopt(p_curl, CURLOPT_TIMEOUT, CURL_OPT_TIMEOUT);
    curl_easy_setopt(p_curl, CURLOPT_FOLLOWLOCATION, CURL_OPT_FOLLOW);
    curl_easy_setopt(p_curl, CURLOPT_MAXREDIRS, CURL_OPT_REDIR);

    /* initialize headers */
    headers = http_headers_init (p_curl);

    curl_result = curl_easy_perform(p_curl);

    curl_easy_cleanup(p_curl);

    curl_slist_free_all(headers);

LABEL_RETURN:
    return curl_result;
}

static int8_t http_init (x_httpc_params *p_x_params,
                         x_httpc_data   *p_x_data)
{
    int ret_val = -1;

    if( (NULL == p_x_params) || (NULL == p_x_data) )
    {
        goto LABEL_RETURN;
    }

    ret_val = 0;

LABEL_RETURN:
    return ret_val;
}

static X_CURL_LIST *http_headers_init (CURL *p_curl)
{
    X_CURL_LIST *headers = NULL;

    if( NULL == p_curl )
    {
        goto LABEL_RETURN;
    }

    headers = curl_slist_append(headers, HTTP_ACCEPT_STRING);
    headers = curl_slist_append(headers, HTTP_CONTENT_TYPE_STRING);

    curl_easy_setopt(p_curl, CURLOPT_HTTPHEADER, headers);

LABEL_RETURN:
    return headers;
}
