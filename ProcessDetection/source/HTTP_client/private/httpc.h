/***************************** C O P Y R I G H T *******************************
*              Copyright (C) 2015. id8 Group Branch Studios Inc.               *
*                            All Rights Reserved.                              *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : httpc.h
Author       : Sharath
Description  : HTTP client wrapper APIs
*******************************************************************************/

#ifndef HTTPC_H
#define HTTPC_H

/******************************* I N C L U D E S ******************************/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <json-c/json.h>

/******************************** D E F I N E S *******************************/

/******************************* T Y P E D E F S ******************************/

typedef enum json_tokener_error E_JSON_ERR;

/************************** E N U M E R A T I O N S ***************************/

/*************************** S T R U C T U R E S ******************************/

typedef struct
{
    char   *url;
} x_httpc_params;

typedef struct
{
    char   *p_in_data;      /* allocated data to be passed */
    char   *p_out_data;     /* NULL to be passed,
                               memory would be allocated
                               caller has to free */
    size_t  in_size;        /* input data size*/
    size_t  out_size;       /* output data size*/
    char    out_result;     /* 0 = success */
} x_httpc_data;

/********************* E X T E R N   V A R I A B L E S ************************/

/***************************** F U N C T I O N S ******************************/
#ifdef    __cplusplus
extern "C" {
#endif
/*******************************************************************************
Function  : http_get
Purpose   : HTTP GET
Arguments : [IN]  p_x_params   pointer to HTTP params (URL, etc)
            [OUT] p_x_data     pointer to HTTP data
Return    : returns 0 if successful, else -1
*******************************************************************************/
int8_t http_get (x_httpc_params *p_x_params, x_httpc_data *p_x_data);

/*******************************************************************************
Function  : http_post
Purpose   : HTTP POST
Arguments : [IN]  p_x_params   pointer to HTTP params (URL, etc)
            [OUT] p_x_data     pointer to HTTP data
Return    : returns 0 if successful, else -1
*******************************************************************************/
int8_t http_post (x_httpc_params *p_x_params, x_httpc_data *p_x_data);

#ifdef    __cplusplus
}
#endif
#endif /* #ifndef HTTPC_H */

/* EOF httpc.h */
