/***************************** C O P Y R I G H T *******************************
*               Copyright (C) 2015. id8 Group Branch Studios Inc.              *
*                             All Rights Reserved.                             *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : detection_main.c
Author       : Neha Mittal
Description  : This file contains implementation for message queues
*******************************************************************************/

/******************************* I N C L U D E S ******************************/
#include <stdio.h>
#include <stdlib.h>

#include "common_header.h"
#include "videoInputWrapper.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

int main( int argc, char* argv[] )
{
    int selected_scheme = 0;

    if( argc < 2 )
    {
        printf(" error: Needed atleast one argument \"ProcessDetection <scheme>\" \n");
    }
    else
    {
        selected_scheme = strtol(argv[1],NULL,10);
    }
    selected_scheme = eTEMPLATE_MATCHING; //eLOGO_DETECTION; //eTEMPLATE_MATCHING;
    printf("selected scheme: %d", selected_scheme);
    switch (selected_scheme)
    {
    case eFINGERPRINTING:
        printf("\n fingerprinting \n");
        algo_process1_thread(NULL);
        break;
    case eLOGO_DETECTION:
        printf("\n Logo Detection \n");
        algo_process2_thread(NULL);
        break;
    case eTEMPLATE_MATCHING:
        printf("\n Template Matching \n");
        algo_process3_thread(NULL);
        break;
    }
}

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/
