/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_context.h
Author       : Sharath
Description  : This file contains declarations for the application's context
*******************************************************************************/

#ifndef _BRANCH_CONTEXT_H_
#define _BRANCH_CONTEXT_H_

/******************************* I N C L U D E S ******************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "branch_types.h"

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

#define NO_OF_HDMI_PORTS    (4)
#define NO_OF_SETUP_PORTS   (3)


/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

typedef enum
{
    E_THREAD_MAIN,
    E_THREAD_BT_SOCK,
    E_THREAD_BT_MSG,
    E_THREAD_IP_SOCK,
    E_THREAD_IP_MSG,
    E_THREAD_VIDEO_DETECT,
    E_THREAD_VIDEO_CAPTURE,
    E_THREAD_MAX
} e_thread_id;

typedef enum
{
    E_QUEUE_MAIN,
    E_QUEUE_BT_SOCK,
    E_QUEUE_BT_MSG,
    E_QUEUE_IP_SOCK,
    E_QUEUE_IP_MSG,
    E_QUEUE_VIDEO_DETECT_MSG,
    E_QUEUE_VIDEO_DETECT_DATA,
	E_QUEUE_VIDEO_DETECT_DATA2,
	E_QUEUE_VIDEO_DETECT_DATA3,
    E_QUEUE_MAX
} e_queue_id;

typedef enum
{
    E_SWITCH_STATE_STANDBY,
    E_SWITCH_STATE_ACTIVE,
    E_SWITCH_STATE_SETUP,
    E_SWITCH_STATE_INVALID
} e_switch_state;

/***************************** S T R U C T U R E S ****************************/

/* Structure to maintain the information of each HDMI port.
   An array of NO_OF_HDMI_PORTS of these have to be maintained.
   Individual instances or All could be exchanged within the eco-system.
   Same information needs to be on the Cloud. */
typedef struct
{
    uint32_t    device_id;      /* should be in database */
    uint8_t     port_number;
    uint8_t     e_hdmi_type;    /* e_hdmi_port_t */
    uint8_t     bm_dev_ctrl;    /* bm_dev_ctrl_t */
    bool_t      b_hdmi_cec;
} x_hdmi_port_info;


typedef struct
{
    void               *p_thread_handle[E_THREAD_MAX];
    void               *p_q_handle[E_QUEUE_MAX];
    void               *p_response[E_QUEUE_MAX];
    x_hdmi_port_info    hdmi_port_info[NO_OF_HDMI_PORTS];
    e_switch_state      e_state;
	uint8_t				curr_hdmi_setup_port;
	uint8_t				get_device_info_retry_count;
} x_switch_context;

/****************************** F U N C T I O N S *****************************/

/*******************************************************************************
Function  : br_get_context

Purpose   : To retrieve the context

Arguments :
            p_x_switch_context pointer to the context

Return    : returns 0 if successful, else -1
*******************************************************************************/
int8_t br_get_context (x_switch_context **p_x_switch_context);

/*******************************************************************************
Function  : br_get_state

Purpose   : To retrieve state of the Application

Arguments :
            p_state pointer to state variable

Return    : returns 0 if successful, else -1
*******************************************************************************/
int8_t br_get_state (uint8_t *p_state);

/*******************************************************************************
Function  : br_set_state

Purpose   : To set the state of Application

Arguments :
            e_state state value

Return    : returns 0 if successful, else -1
*******************************************************************************/
int8_t br_set_state (uint8_t e_state);

/*******************************************************************************
Function  : br_get_resp_msg

Purpose   : To retrieve the response message

Arguments : br_get_resp_msg pointer to response message
            e_q_id          queue identifier

Return    : returns 0 if successful, else -1
*******************************************************************************/
int8_t br_get_resp_msg (void **p_x_br_pvt_msg, e_queue_id e_q_id);

#endif /* #ifndef _BRANCH_CONTEXT_H_ */

/* EOF context.h */
