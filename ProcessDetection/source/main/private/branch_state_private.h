/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
File name   : branch_state_private.h
Author      : RAHUL NAKHATE
Description : This file contains private declarations for main states handling
*******************************************************************************/

#ifndef _BRANCH_STATE_PRIVATE_H
#define _BRANCH_STATE_PRIVATE_H

/******************************* I N C L U D E S ******************************/

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

typedef enum
{
	E_TIMER_STATE_VIDEO_DETECT = 0,
	E_TIMER_STATE_GET_DEVICE_INFO,

	E_TIMER_STATE_MAX
} e_timer_state_t;

/***************************** S T R U C T U R E S ****************************/

/****************************** F U N C T I O N S *****************************/

/*******************************************************************************
Function     : parse_standby_state_pvt_msg
Description  : Handles main thread standby state messages
Arguments    : Pointer to received pvt message
			   Pointer to response pvt message
Return value : 0 Success -ve Failure
*******************************************************************************/
int8_t parse_standby_state_pvt_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
									x_branch_pvt_message *px_resp_pvt_msg);

/*******************************************************************************
Function     : parse_active_state_pvt_msg
Description  : Handles main thread active state messages
Arguments    : Pointer to received pvt message
			   Pointer to response pvt message
Return value : 0 Success -ve Failure
*******************************************************************************/
int8_t parse_active_state_pvt_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
								   x_branch_pvt_message *px_resp_pvt_msg);

/*******************************************************************************
Function     : parse_setup_state_pvt_msg
Description  : Handles main thread setup state messages
Arguments    : Pointer to received pvt message
			   Pointer to response pvt message
Return value : 0 Success -ve Failure
*******************************************************************************/
int8_t parse_setup_state_pvt_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
								  x_branch_pvt_message *px_resp_pvt_msg);

/*******************************************************************************
Function     : br_start_timer
Description  : Startes the timer for given time out value
Arguments    : Time out in ms
			   State
Return value : 0 Success -ve Failure
*******************************************************************************/
int8_t br_start_timer (uint32_t time_out_ms, e_timer_state_t state);

/*******************************************************************************
Function     : br_stop_timer
Description  : Stops the running timer
Arguments    : None
Return value : 0 Success -ve Failure
*******************************************************************************/
int8_t br_stop_timer (void);

/*******************************************************************************
Function     : set_video_detect_time_out_state
Description  : Sets the video detect time out flag
Arguments    : Flag
Return value : None
*******************************************************************************/
void set_video_detect_time_out_state (bool_t flag);

#endif /* _BRANCH_STATE_PRIVATE_H */

/* EOF branch_state_private.h */
