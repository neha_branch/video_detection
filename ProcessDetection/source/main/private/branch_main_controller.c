/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_main_controller.c
Author       : Sharath
Description  : This file contains implementation for StateMachine related
               functionalities of SwitchModules
*******************************************************************************/

/******************************* I N C L U D E S ******************************/

#include "branch_pvt_messages.h"
#include "branch_state_private.h"
#include "branch_main_controller.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

int8_t br_main_state_machine (x_branch_pvt_message *p_x_br_pvt_msg)
{
    int        ret_val = -1;

    x_switch_context     *p_x_switch_context = NULL;
    x_branch_pvt_message *p_x_rsp_pvt_msg    = NULL;

    if( NULL == p_x_br_pvt_msg )
    {
        goto LABEL_RETURN;
    }

    /* get context */
    (void)br_get_context (&p_x_switch_context);
    if( NULL == p_x_switch_context )
    {
        ret_val = -2;
        goto LABEL_RETURN;
    }

    /* validate message */
    ret_val = validate_pvt_msg (p_x_br_pvt_msg);
    if( 0 != ret_val )
    {
        goto LABEL_RETURN;
    }

    /* get response message holder */
    ret_val = br_get_resp_msg ((void **)&p_x_rsp_pvt_msg, E_QUEUE_MAIN);
    if( (0 != ret_val) || (NULL == p_x_rsp_pvt_msg) )
    {
        goto LABEL_RETURN;
    }

    /* if the message is not for this module,
       post it to the appropriate queue ... should not happen */
    /* TODO */

    switch( p_x_switch_context->e_state )
    {
        case E_SWITCH_STATE_STANDBY:
        {
            parse_standby_state_pvt_msg (p_x_br_pvt_msg, p_x_rsp_pvt_msg);
        }
        break;

        case E_SWITCH_STATE_ACTIVE:
        {
            parse_active_state_pvt_msg (p_x_br_pvt_msg, p_x_rsp_pvt_msg);
        }
        break;

        case E_SWITCH_STATE_SETUP:
        {
            parse_setup_state_pvt_msg (p_x_br_pvt_msg, p_x_rsp_pvt_msg);
        }
        break;

        default:
        {
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }

    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/

