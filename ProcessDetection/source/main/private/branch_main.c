/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_main.c
Author       : Sharath
Description  : This file contains implementation for initialization of
               SwitchModules and is the main controller
*******************************************************************************/

/******************************* I N C L U D E S ******************************/

#include <time.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>

#include "branch_context.h"
#include "branch_debug_print.h"
#include "branch_utils.h"
//#include "branch_video_detect.h"
#include "branch_pvt_messages.h"
#include "branch_state_private.h"
#include "branch_main_controller.h"
//#include "cloud_ip_stream_server.h"
//#include "branch_ip_ctrl.h"

#include <time.h>
#include <unistd.h>

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

//#define ENABLE_IP_MODULE
//#define ENABLE_BT_MODULE
#define ENABLE_VIDEO_MODULE

#ifndef WIN32
typedef HANDLE (void*);
typedef uint32_t DWORD;

#endif

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

static x_switch_context x_switch_cntxt;
static clock_t start_clock;
static uint32_t	time_out = 0;
static e_timer_state_t timer_state = E_TIMER_STATE_MAX;

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

static int8_t switch_init (void);

static int8_t create_switch_threads (void);

static int8_t br_main_proc (x_branch_pvt_message *p_x_br_pvt_msg);

static int8_t send_msg ();

static void br_handle_timeout (void);
static void handle_timer (void);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

/*int8_t main (void)
{
    int8_t ret_val = -1;
    time_t start, end;
    double time_elapsed = 0;
    start = clock();
    while (time_elapsed < 1.5)
    {
    	double temp = CLOCKS_PER_SEC;
    	sleep(1);
    	end = clock();
    	time_elapsed = ((double)(end-start)/CLOCKS_PER_SEC);
    }

    x_switch_cntxt.e_state = E_SWITCH_STATE_STANDBY;
    ret_val = switch_init ();
    if(0 != ret_val)
    	printf("\nswitch_init() failed()");

     forever loop
    while( 1 )
    {
        x_branch_pvt_message *p_x_br_pvt_msg = NULL;

         get a message from the queue
        ret_val = br_recv_msg (E_QUEUE_MAIN, (void **)&p_x_br_pvt_msg, NULL, 0);
        if( 0 == ret_val )
        {
             process message
            ret_val = br_main_proc (p_x_br_pvt_msg);

             message used ... free it
            ret_val = br_free_msg (E_QUEUE_MAIN);

             send message if required
            send_msg ();
        }

		 Check timer
		handle_timer ();

        sleep (THREAD_SLEEP_HIGH);
    }

    return 0;
}*/

int8_t br_get_context (x_switch_context **p_x_switch_context)
{
    int        ret_val = -1;

    if( NULL == p_x_switch_context )
    {
        goto LABEL_RETURN;
    }

    *p_x_switch_context = (x_switch_context *)&x_switch_cntxt;

    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}

int8_t br_get_resp_msg (void **p_x_br_pvt_msg, e_queue_id e_q_id)
{
    int        ret_val = -1;

    if( (NULL == p_x_br_pvt_msg) || (E_QUEUE_MAX <= e_q_id) )
    {
        goto LABEL_RETURN;
    }

    *p_x_br_pvt_msg = (x_branch_pvt_message *)x_switch_cntxt.p_response[e_q_id];

    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}

int8_t br_get_state (uint8_t *p_state)
{
    int        ret_val = -1;

    if( NULL == p_state )
    {
        goto LABEL_RETURN;
    }

    *p_state = x_switch_cntxt.e_state;

    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}

int8_t br_set_state (uint8_t e_state)
{
    int        ret_val = -1;

    if( E_SWITCH_STATE_INVALID <= e_state )
    {
        goto LABEL_RETURN;
    }

    x_switch_cntxt.e_state = e_state;

    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}

int8_t br_start_timer (uint32_t time_out_ms, e_timer_state_t state)
{
	start_clock = clock ();
	time_out	= time_out_ms;
	timer_state = state;

	return (0);
}

int8_t br_stop_timer (void)
{
	time_out	= 0;
	timer_state = E_TIMER_STATE_MAX;

	return (0);
}

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/

/*static int8_t switch_init (void)
{
    int        ret_val = -1;

    void   *q_handle        = NULL;

     initialize context
    (void)memset(&x_switch_cntxt, 0, sizeof(x_switch_cntxt));

    x_switch_cntxt.p_thread_handle[E_THREAD_MAIN] = (void *)&main;

     create it's own message queue
    q_handle = br_create_q (E_QUEUE_MAIN,
                            QUEUE_SIZE_MED,
                            MESSAGE_SIZE_MAX,
                            0);
    if( NULL == q_handle )
    {
        ret_val = -2;
        goto LABEL_RETURN;
    }

     create a placeholder for response message
    x_switch_cntxt.p_response[E_QUEUE_MAIN] =
                            (void *)malloc(sizeof(x_branch_pvt_message));

     create threads
    ret_val = create_switch_threads ();

#ifdef TEMP_MSG_TEST
    {
        x_branch_pvt_message x_br_pvt_msg;
        uint32_t  _min_payload[BRANCH_PVT_MIN_PAYLOAD_SIZE] = {2, 0};

        Sleep (5000);
        create_pvt_msg (&x_br_pvt_msg, E_PVT_MSG_TYPE_REQUEST,
                        E_PVT_MSG_START_VIDEO_DETECT, _min_payload, 0, NULL);

        (void)br_send_msg (E_QUEUE_IP_SOCK, &x_br_pvt_msg, NULL, 0);
    }
#endif

LABEL_RETURN:
    return (ret_val);
}*/

/*static int8_t create_switch_threads (void)
{
	int	h_thread		= 0;
    pthread_t	dwThreadId		= 0x00;

    int        ret_val = -1;

     frame data thread
    h_thread = pthread_create (
				&dwThreadId,
				NULL,
				br_ip_thread,   thread function name
				0                        use default creation flags
				);             returns the thread identifier
	if( h_thread )
	{
		ret_val = -4;
		goto LABEL_RETURN;
	}

	h_thread = pthread_create (
				&dwThreadId,
				NULL,
				cloud_server_control_thread,
				0
				);
	if( h_thread )
	{
		ret_val = -4;
		goto LABEL_RETURN;
	}

#ifdef ENABLE_VIDEO_MODULE
     Video detection thread
	h_thread = pthread_create (
			&dwThreadId,
            NULL,
            br_video_detect_thread,   thread function name
            0                        use default creation flags
            );             returns the thread identifier
    if( h_thread )
    {
        ret_val = -4;
        goto LABEL_RETURN;
    }
    x_switch_cntxt.p_thread_handle[E_THREAD_VIDEO_DETECT] = (void *)dwThreadId;
#endif
    ret_val = 0;

LABEL_RETURN:
	if(0 != ret_val)
		printf("\nError in create_switch_threads()");
    return (ret_val);
}

static int8_t br_main_proc (x_branch_pvt_message *p_x_br_pvt_msg)
{
    int        ret_val = -1;

    if( NULL == p_x_br_pvt_msg )
    {
        goto LABEL_RETURN;
    }

     initialize message to be used & sent later
       (for request, response or event)
    init_send_msg (p_x_br_pvt_msg, E_QUEUE_MAIN);
    
     check state of the module
    br_main_state_machine (p_x_br_pvt_msg);

    ret_val = 0;

LABEL_RETURN:
    return (ret_val);
}*/

static int8_t send_msg ()
{
    x_branch_pvt_message *p_x_resp_pvt_msg = NULL;

    int        ret_val = -1;

    ret_val = br_get_resp_msg ((void **)&p_x_resp_pvt_msg, E_QUEUE_MAIN);
    if( 0 != ret_val )
    {
        goto LABEL_RETURN;
    }
    
    if( NULL == p_x_resp_pvt_msg )
    {
        ret_val = -2;
        goto LABEL_RETURN;
    }

    if( E_PVT_MSG_TYPE_NONE == p_x_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt )
    {
        /* nothing to send, probably a response or event was handled */
        ret_val = 0;
        goto LABEL_RETURN;
    }

    p_x_resp_pvt_msg->x_pvt_msg_hdr.msg_checksum =
                            calculate_pvt_msg_checksum (p_x_resp_pvt_msg);
    if( 0 == p_x_resp_pvt_msg->x_pvt_msg_hdr.msg_checksum )
    {
        goto LABEL_RETURN;
    }
    
    ret_val = br_send_msg (p_x_resp_pvt_msg->x_pvt_msg_hdr.e_destination,
                           p_x_resp_pvt_msg, NULL, 0);

LABEL_RETURN:
    return (ret_val);
}

static void
handle_timer (void)
{
	clock_t diff_clock, curr_clock;
	uint32_t diff_ms = 0;

	if (0 == time_out)
	{
		goto LABEL_RETURN;
	}

	curr_clock = clock ();
	diff_clock = curr_clock - start_clock;
	diff_ms = diff_clock * 1000 / CLOCKS_PER_SEC;
	
	if (diff_ms > time_out)
	{
		/* Time out */
		br_handle_timeout ();
		/* Reset timer */
		br_stop_timer ();
	}

LABEL_RETURN:
	return;
}

static void
br_handle_timeout (void)
{
	x_branch_pvt_message x_pvt_msg;

	switch (timer_state)
	{
		case E_TIMER_STATE_VIDEO_DETECT:
		{
			/* Send stop video detect */
			debug_printf (E_DEBUG_LEVEL_INFO,
						  "Time out occured for video detection");
			
			/* Set time out state */
			set_video_detect_time_out_state (true);

			create_pvt_msg (&x_pvt_msg,
							E_PVT_MSG_TYPE_REQUEST,
							E_PVT_MSG_STOP_VIDEO_DETECT,
							NULL,
							0,
							NULL,
							E_QUEUE_MAIN,
							E_QUEUE_VIDEO_DETECT_MSG);
		}
		break;

		case E_TIMER_STATE_GET_DEVICE_INFO:
		{
			x_switch_context *px_switch_contex;
			uint32_t min_payload[2] = {0, 0};
			int8_t ret_val = 0;

			debug_printf (E_DEBUG_LEVEL_INFO,
						  "Retrying get device info");

			ret_val = br_get_context (&px_switch_contex);
			if (NULL == px_switch_contex)
			{
				goto LABEL_RETURN;
			}
			min_payload[0] = px_switch_contex->curr_hdmi_setup_port;
			create_pvt_msg (&x_pvt_msg,
							E_PVT_MSG_TYPE_REQUEST,
							E_PVT_MSG_GET_DEVICE_INFO,
							min_payload,
							0,
							NULL,
							E_QUEUE_MAIN,
							E_QUEUE_BT_MSG);
		}
		break;

		default:
		{
			debug_printf (E_DEBUG_LEVEL_DEBUG, "Invalid main timer state");
		}
	}

	/* Send pvt msg */
	br_send_msg (x_pvt_msg.x_pvt_msg_hdr.e_destination, &x_pvt_msg, NULL, 0);

LABEL_RETURN:
	return;
}
