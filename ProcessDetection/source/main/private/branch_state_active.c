/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
File name   : branch_state_active.c
Author      : RAHUL NAKHATE
Description : This file contains implementation for handling active state
			  private messaging
*******************************************************************************/

/******************************* I N C L U D E S ******************************/

#include "branch_types.h"
#include "branch_utils.h"
#include "branch_device_info.h"
#include "branch_pvt_messages.h"
#include "branch_state_private.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

static int8_t
handle_active_req_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					   x_branch_pvt_message *px_resp_pvt_msg);
static int8_t
handle_active_resp_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					    x_branch_pvt_message *px_resp_pvt_msg);
static int8_t
handle_active_event_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					     x_branch_pvt_message *px_resp_pvt_msg);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

int8_t parse_active_state_pvt_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
								   x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}
	
	switch (px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type)
    {
		case E_PVT_MSG_TYPE_REQUEST:
        {
            ret_val = handle_active_req_msg (px_rcvd_pvt_msg, px_resp_pvt_msg);
        }
        break;
		case E_PVT_MSG_TYPE_RESPONSE:
        {
            ret_val = handle_active_resp_msg (px_rcvd_pvt_msg, px_resp_pvt_msg);
        }
        break;
		case E_PVT_MSG_TYPE_EVENT:
        {
            ret_val = handle_active_event_msg (px_rcvd_pvt_msg, px_resp_pvt_msg);
        }
        break;

        default:
        {
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }

LABEL_RETURN:
	return (ret_val);
}  /* parse_active_state_pvt_msg () */

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/

static int8_t
handle_active_req_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					   x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}

	px_resp_pvt_msg->x_pvt_msg_hdr.e_source = E_QUEUE_MAIN;
	px_resp_pvt_msg->x_pvt_msg_hdr.e_destination =
								px_rcvd_pvt_msg->x_pvt_msg_hdr.e_source;
	px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type =
								E_PVT_MSG_TYPE_RESPONSE;
	px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt =
								px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt;

	switch (px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt)
    {
		case E_PVT_MSG_START_SETUP:
		{
			x_branch_pvt_message x_send_msg;
			x_switch_context *px_switch_contex;
			uint32_t min_payload[2] = {0, 0};

			/* Enter to setup mode
			   Change main state to Setup */
			ret_val = br_get_context (&px_switch_contex);
			if (NULL == px_switch_contex)
			{
				goto LABEL_RETURN;
			}

			/* Change state to setup */
			br_set_state (E_SWITCH_STATE_SETUP);
			/* Save current HDMI device to context */
			px_switch_contex->curr_hdmi_setup_port =
					px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[0] - 1;

			/* Send the Success response */
			px_resp_pvt_msg->x_pvt_msg_hdr.min_payload[0] = 0;

			/* Reinit get device info retry count */
			px_switch_contex->get_device_info_retry_count = 0;

			/* Start HDMI detection
			   Send get device info msg */
			min_payload[0] = px_switch_contex->curr_hdmi_setup_port;
			ret_val = create_pvt_msg (&x_send_msg,
									  E_PVT_MSG_TYPE_REQUEST,
									  E_PVT_MSG_GET_DEVICE_INFO,
									  min_payload,
									  0,
									  NULL,
									  E_QUEUE_MAIN,
									  E_QUEUE_BT_MSG);
			
			ret_val = br_send_msg (x_send_msg.x_pvt_msg_hdr.e_destination,
								   &x_send_msg, NULL, 0);
		}
		break;

		case E_PVT_MSG_SET_SOURCE:
		{
			x_hdmi_port_info device_info;
			uint32_t min_payload[2] = {0, 0};

			/* Get the HDMI port for this device */
			get_device_info (&device_info,
							 px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[0] - 1);
			min_payload[0] = device_info.port_number;

			/* Forward this to BT */
			create_pvt_msg (px_resp_pvt_msg,
							E_PVT_MSG_TYPE_REQUEST,
							E_PVT_MSG_SET_SOURCE,
							min_payload,
							0,
							NULL,
							E_QUEUE_MAIN,
							E_QUEUE_BT_MSG);
		}
		break;

		case E_PVT_MSG_GET_SOURCE:
		{
			/* Forward this to BT */
			memcpy (px_resp_pvt_msg,
					px_rcvd_pvt_msg,
					get_pvt_msg_size (px_rcvd_pvt_msg));

			px_resp_pvt_msg->x_pvt_msg_hdr.e_destination = E_QUEUE_BT_MSG;
			px_resp_pvt_msg->x_pvt_msg_hdr.e_source		 = E_QUEUE_MAIN;
		}
		break;

		case E_PVT_MSG_GET_DEVICE_INFO:
		{
			get_device_info ((x_hdmi_port_info *) px_resp_pvt_msg->payload,
						px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[0] - 1);
			px_resp_pvt_msg->x_pvt_msg_hdr.payload_size =
						sizeof (x_hdmi_port_info);
		}
		break;

		case E_PVT_MSG_GET_DEVICE_INFO_LIST:
		{
			get_device_info_list ((x_hdmi_port_info *) px_resp_pvt_msg->payload);
			px_resp_pvt_msg->x_pvt_msg_hdr.payload_size =
						sizeof (x_hdmi_port_info) * NO_OF_HDMI_PORTS;
		}
		break;

		case E_PVT_MSG_SET_DEVICE_ID:
		{
			x_hdmi_port_info x_device_info;
			uint8_t device = 0;
			uint8_t remove_device = false;

			/* Update the device ID */
			device = (px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[1] - 1);
			get_device_info (&x_device_info, device);
			x_device_info.device_id =
							px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[0];
			if (0x0000 == x_device_info.device_id)
			{
				remove_device = true;
			}
			set_device_info (&x_device_info, device, remove_device);

			/* Send updated info to Neo */
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type =
											E_PVT_MSG_TYPE_REQUEST;
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt =
											E_PVT_MSG_SET_DEVICE_INFO_LIST;
			get_device_info_list ((x_hdmi_port_info *)
									px_resp_pvt_msg->payload);
			px_resp_pvt_msg->x_pvt_msg_hdr.payload_size =
								sizeof (x_hdmi_port_info) * NO_OF_HDMI_PORTS;

			px_resp_pvt_msg->x_pvt_msg_hdr.e_source = E_QUEUE_MAIN;
			px_resp_pvt_msg->x_pvt_msg_hdr.e_destination = E_QUEUE_BT_MSG;
			/* once get the resp from BT, send resp to IP */
		}
		break;

        default:
        {
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt = E_PVT_MSG_NONE;
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }

LABEL_RETURN:
	return (ret_val);
}  /* handle_active_req_msg () */

static int8_t
handle_active_resp_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					    x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}

	switch (px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt)
    {
		case E_PVT_MSG_SET_DEVICE_INFO_LIST:
		{
			/* If resp from BT, send to IP */
			if (E_QUEUE_BT_MSG == px_rcvd_pvt_msg->x_pvt_msg_hdr.e_source)
			{
				/* Send current device list */
				px_resp_pvt_msg->x_pvt_msg_hdr.e_destination = E_QUEUE_IP_MSG;
				px_resp_pvt_msg->x_pvt_msg_hdr.e_source = E_QUEUE_MAIN;
				px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type =
											E_PVT_MSG_TYPE_RESPONSE;
				px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt =
											E_PVT_MSG_GET_DEVICE_INFO_LIST;

				get_device_info_list ((x_hdmi_port_info *)
											px_resp_pvt_msg->payload);
				px_resp_pvt_msg->x_pvt_msg_hdr.payload_size =
								(sizeof (x_hdmi_port_info) * NO_OF_HDMI_PORTS);
			}
		}
		break;

		case E_PVT_MSG_GET_DEVICE_INFO_LIST:
		{
			/* If resp from BT, save to context */
			if (E_QUEUE_BT_MSG == px_rcvd_pvt_msg->x_pvt_msg_hdr.e_source)
			{
				set_device_info_list ((x_hdmi_port_info *)
											px_rcvd_pvt_msg->payload);
			}
		}
		break;

		case E_PVT_MSG_GET_SOURCE:
		{
			/* Forward this to BT */
			x_hdmi_port_info x_device_info;
			uint32_t min_payload[2] = {0, 0};

			if (0xFF == px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[1])
			{
				min_payload[1] = 0xFFFFFFFF;
			}
			else
			{
				get_device_info (&x_device_info,
						(px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[1] - 1));
				min_payload[1] = x_device_info.device_id;
			}
			
			min_payload[0] = px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[0];

			create_pvt_msg (px_resp_pvt_msg,
							E_PVT_MSG_TYPE_RESPONSE,
							E_PVT_MSG_GET_SOURCE,
							min_payload,
							0,
							NULL,
							E_QUEUE_MAIN,
							E_QUEUE_IP_MSG);
		}
		break;

        default:
        {
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt = E_PVT_MSG_NONE;
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }

LABEL_RETURN:
	return (ret_val);
}  /* handle_active_resp_msg () */

static int8_t
handle_active_event_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					     x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}
	
	switch (px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt)
    {
		case E_PVT_EVT_BT_DISCONNECTED:
		{
			/* Goto standby */
			x_switch_context *px_context = NULL;

			ret_val = br_get_context (&px_context);
			if (NULL == px_context)
			{
				ret_val = -1;
				goto LABEL_RETURN;
			}
			px_context->e_state = E_SWITCH_STATE_STANDBY;

			/* Send switch not available event */
			ret_val = create_pvt_msg (px_resp_pvt_msg,
									  E_PVT_MSG_TYPE_EVENT, 
									  E_PVT_EVT_SWITCH_NOT_AVAILABLE,
									  NULL,
									  0,
									  NULL,
									  E_QUEUE_MAIN,
									  E_QUEUE_IP_MSG);
		}
		break;

        default:
        {
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt = E_PVT_MSG_NONE;
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }
LABEL_RETURN:
	return (ret_val);
}  /* handle_active_event_msg () */

/* EOF branch_state_active.c */
