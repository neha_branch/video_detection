/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
File name   : branch_device_info.h
Author      : RAHUL NAKHATE
Description : This file has defines related to device info
*******************************************************************************/

#ifndef _BRANCH_DEVICE_INFO_H
#define _BRANCH_DEVICE_INFO_H

/******************************* I N C L U D E S ******************************/

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/

/****************************** F U N C T I O N S *****************************/

/*******************************************************************************
Function     : init_device_info
Description  : Intializes the context HDMI port info from the device info file
Arguments    : None
Return value : 0 : Success -ve : Failure
*******************************************************************************/
int8_t init_device_info (void);

/*******************************************************************************
Function     : set_device_info
Description  : Saves the given HDMI port info to context and file
Arguments    : Pointer to hdmi port info
			   Device index
			   Remove flag
Return value : 0 : Success -ve : Failure
*******************************************************************************/
int8_t set_device_info (x_hdmi_port_info *px_device_info,
						uint8_t index, uint8_t remove);

/*******************************************************************************
Function     : get_device_info
Description  : Returns the given HDMI port info from context
Arguments    : Pointer to hdmi port info
			   Device index
Return value : 0 : Success -ve : Failure
*******************************************************************************/
int8_t get_device_info (x_hdmi_port_info *px_device_info, uint8_t index);

/*******************************************************************************
Function     : set_device_info_list
Description  : Saves the HDMI port info to context and file
Arguments    : Pointer to hdmi port info list
Return value : 0 : Success -ve : Failure
*******************************************************************************/
int8_t set_device_info_list (x_hdmi_port_info *px_device_info_list);

/*******************************************************************************
Function     : get_device_info_list
Description  : Copies the HDMI port info from the context to given list
Arguments    : Pointer to hdmi port info list
Return value : 0 : Success -ve : Failure
*******************************************************************************/
int8_t get_device_info_list (x_hdmi_port_info *px_device_info_list);

#endif /* _BRANCH_DEVICE_INFO_H */

/* EOF branch_device_info.h */
