/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : branch_main_controller.h
Author       : Sharath
Description  : This file contains declarations for main controller module
*******************************************************************************/

#ifndef _BRANCH_MAIN_CONTROLLER_H_
#define _BRANCH_MAIN_CONTROLLER_H_

/******************************* I N C L U D E S ******************************/

#include <stdio.h>
#include <stdlib.h>

#include "branch_types.h"

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/

/****************************** F U N C T I O N S *****************************/

/*******************************************************************************
Function  : br_main_state_machine

Purpose   : Message handling based on state

Arguments :
            x_branch_pvt_message pointer to message

Return    : returns 0 if successful, else -1
*******************************************************************************/
int8_t br_main_state_machine (x_branch_pvt_message *p_x_br_pvt_msg);


#endif /* #ifndef _BRANCH_MAIN_CONTROLLER_H_ */

/* EOF context.h */
