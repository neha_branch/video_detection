/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
File name   : branch_device_info.c
Author      : RAHUL NAKHATE
Description : This file contains the set of functions related to hdmi device
			  info
*******************************************************************************/

/******************************* I N C L U D E S ******************************/
#include <stdbool.h>
#include "branch_types.h"
#include "branch_context.h"
#include "branch_device_info.h"
#include "branch_pvt_messages.h"
#include "branch_utils.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

#define DEVICE_INFO_FILE	"device_info.bin"

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

static int8_t
read_device_info_from_file (x_hdmi_port_info *px_device_info_list);
static int8_t
write_device_info_to_file (x_hdmi_port_info *px_device_info_list);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

int8_t
init_device_info (void)
{
	int8_t ret_val = 0;
	x_switch_context *px_switch_context = NULL;

	ret_val = br_get_context (&px_switch_context);
	if (NULL == px_switch_context)
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}

#ifdef LOCAL_FILE_CONTEXT
    ret_val = read_device_info_from_file (px_switch_context->hdmi_port_info);
#else
    /* Send E_PVT_MSG_GET_DEVICE_INFO_LIST to BT */
    {
        x_branch_pvt_message x_pvt_msg;
		create_pvt_msg (&x_pvt_msg,
						E_PVT_MSG_TYPE_REQUEST,
                        E_PVT_MSG_GET_DEVICE_INFO_LIST,
						NULL,
						0,
						NULL,
						E_QUEUE_MAIN,
                        E_QUEUE_BT_MSG);
	    /* Send pvt msg */
	    br_send_msg (x_pvt_msg.x_pvt_msg_hdr.e_destination,
                     &x_pvt_msg, NULL, 0);
    }
#endif

LABEL_RETURN:
	return (ret_val);
}  /* init_device_info () */

int8_t
set_device_info (x_hdmi_port_info *px_device_info, uint8_t index, uint8_t remove)
{
	x_switch_context *px_switch_context = NULL;
	int8_t ret_val = 0;

	if (NULL == px_device_info)
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}
	
	ret_val = br_get_context (&px_switch_context);
	if (NULL == px_switch_context)
	{
		ret_val = -2;
		goto LABEL_RETURN;
	}

	if (true == remove)
	{
		uint8_t i = 0;
		/* Remove flag is set
		   Remove this device */
		memset (&px_switch_context->hdmi_port_info[index],
				0,
				sizeof (x_hdmi_port_info));
		/* Move not configured device to last index */
		for (i = index; i < (NO_OF_SETUP_PORTS - 1); i++)
		{
			x_hdmi_port_info temp_device;

			memcpy (&temp_device,
					&px_switch_context->hdmi_port_info[i],
					sizeof (x_hdmi_port_info));
			memcpy (&px_switch_context->hdmi_port_info[i],
					&px_switch_context->hdmi_port_info[i+1],
					sizeof (x_hdmi_port_info));
			memcpy (&px_switch_context->hdmi_port_info[i+1],
					&temp_device,
					sizeof (x_hdmi_port_info));
		}
	}
	else
	{
		memcpy (&px_switch_context->hdmi_port_info[index],
				px_device_info,
				sizeof (x_hdmi_port_info));
	}

	ret_val = write_device_info_to_file (px_switch_context->hdmi_port_info);

LABEL_RETURN:
	return (ret_val);
}  /* set_device_info () */

int8_t
get_device_info (x_hdmi_port_info *px_device_info, uint8_t index)
{
	x_switch_context *px_switch_context = NULL;
	int8_t ret_val = 0;

	if (NULL == px_device_info)
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}
	
	ret_val = br_get_context (&px_switch_context);
	if (NULL == px_switch_context)
	{
		ret_val = -2;
		goto LABEL_RETURN;
	}

	memcpy (px_device_info,
			&px_switch_context->hdmi_port_info[index],
			sizeof (x_hdmi_port_info));
	
LABEL_RETURN:
	return (ret_val);
}  /* get_device_info () */

int8_t
set_device_info_list (x_hdmi_port_info *px_device_info_list)
{
	x_switch_context *px_switch_context = NULL;
	int8_t ret_val = 0;

	if (NULL == px_device_info_list)
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}
	
	ret_val = br_get_context (&px_switch_context);
	if (NULL == px_switch_context)
	{
		ret_val = -2;
		goto LABEL_RETURN;
	}

	memcpy (px_switch_context->hdmi_port_info,
			px_device_info_list,
			sizeof (x_hdmi_port_info) * NO_OF_HDMI_PORTS);

	ret_val = write_device_info_to_file (px_switch_context->hdmi_port_info);

LABEL_RETURN:
	return (ret_val);
}  /* set_device_info_list () */

int8_t
get_device_info_list (x_hdmi_port_info *px_device_info_list)
{
	x_switch_context *px_switch_context = NULL;
	int8_t ret_val = 0;

	if (NULL == px_device_info_list)
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}
	
	ret_val = br_get_context (&px_switch_context);
	if (NULL == px_switch_context)
	{
		ret_val = -2;
		goto LABEL_RETURN;
	}

	memcpy (px_device_info_list,
			px_switch_context->hdmi_port_info,
			sizeof (x_hdmi_port_info) * NO_OF_HDMI_PORTS);
	
LABEL_RETURN:
	return (ret_val);
}  /* get_device_info_list () */

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/

static int8_t
read_device_info_from_file (x_hdmi_port_info *px_device_info_list)
{
	int8_t ret_val = 0;
    FILE *p_in_file = NULL;

	if (NULL == px_device_info_list)
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}

	p_in_file = fopen (DEVICE_INFO_FILE, "rb");
	if (NULL == p_in_file)
	{
		ret_val = -2;
		goto LABEL_RETURN;
	}

	fread (px_device_info_list,
		   sizeof (x_hdmi_port_info) * NO_OF_HDMI_PORTS,
		   1,
		   p_in_file);
	
	fclose (p_in_file);    

LABEL_RETURN:
	return (ret_val);
}  /* read_device_info_from_file () */

static int8_t
write_device_info_to_file (x_hdmi_port_info *px_device_info_list)
{
	int8_t ret_val = 0;
	FILE *p_out_file = NULL;

	if (NULL == px_device_info_list)
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}

	p_out_file = fopen (DEVICE_INFO_FILE, "wb");
    if (NULL == p_out_file)
    {
		ret_val = -2;
		goto LABEL_RETURN;
    }

	fwrite (px_device_info_list,
			sizeof (x_hdmi_port_info) * NO_OF_HDMI_PORTS,
			1,
			p_out_file);

	fclose (p_out_file);

LABEL_RETURN:
	return (ret_val);
}  /* write_device_info_to_file () */

/* EOF branch_device_info.c */
