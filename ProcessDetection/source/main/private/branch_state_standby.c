/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
File name   : branch_state_standby.c
Author      : RAHUL NAKHATE
Description : This file contains implementation for handling standby state
			  private messaging
*******************************************************************************/

/******************************* I N C L U D E S ******************************/

#include "branch_types.h"
#include "branch_utils.h"
#include "branch_device_info.h"
#include "branch_pvt_messages.h"
#include "branch_state_private.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

static int8_t handle_standby_req_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
									  x_branch_pvt_message *px_resp_pvt_msg);
static int8_t handle_standby_resp_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
									   x_branch_pvt_message *px_resp_pvt_msg);
static int8_t handle_standby_event_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
										x_branch_pvt_message *px_resp_pvt_msg);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

int8_t parse_standby_state_pvt_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
									x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}
	
	switch (px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type)
    {
		case E_PVT_MSG_TYPE_REQUEST:
        {
            ret_val = handle_standby_req_msg (px_rcvd_pvt_msg, px_resp_pvt_msg);
        }
        break;
		case E_PVT_MSG_TYPE_RESPONSE:
        {
            ret_val = handle_standby_resp_msg (px_rcvd_pvt_msg,
											   px_resp_pvt_msg);
        }
        break;
		case E_PVT_MSG_TYPE_EVENT:
        {
            ret_val = handle_standby_event_msg (px_rcvd_pvt_msg,
												px_resp_pvt_msg);
        }
        break;

        default:
        {
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }

LABEL_RETURN:
	return (ret_val);
}  /* parse_standby_state_pvt_msg () */

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/

static int8_t
handle_standby_req_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					    x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}

	/* Device is not ready yet,
	   Send device not ready response */
	px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type =
								E_PVT_MSG_TYPE_RESPONSE;
	px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt =
								px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt;
	px_resp_pvt_msg->x_pvt_msg_hdr.min_payload[0] = E_PVT_MSG_RSP_NOT_READY;
	px_resp_pvt_msg->x_pvt_msg_hdr.e_destination =
								px_rcvd_pvt_msg->x_pvt_msg_hdr.e_source;
	px_resp_pvt_msg->x_pvt_msg_hdr.e_source = E_QUEUE_MAIN;

LABEL_RETURN:
	return (ret_val);
}  /* handle_standby_req_msg () */

static int8_t
handle_standby_resp_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					     x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}

	
LABEL_RETURN:
	return (ret_val);
}  /* handle_standby_resp_msg () */

static int8_t
handle_standby_event_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
						  x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}
	
	switch (px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt)
    {
		case E_PVT_EVT_BT_CONNECTED:
		{
			/* Come out of standby */
			x_switch_context *px_context = NULL;

			/* Send switch available event */
			ret_val = create_pvt_msg (px_resp_pvt_msg,
									  E_PVT_MSG_TYPE_EVENT,
									  E_PVT_EVT_SWITCH_AVAILABLE,
									  NULL,
									  0,
									  NULL,
									  E_QUEUE_MAIN,
									  E_QUEUE_IP_MSG);

			ret_val = br_get_context (&px_context);
			if (NULL == px_context)
			{
				ret_val = -1;
				goto LABEL_RETURN;
			}
			px_context->e_state = E_SWITCH_STATE_ACTIVE;

			/* initialize the hdmi port info */
			ret_val = init_device_info ();

			if (NO_ERROR != ret_val)
			{
				/* Device is not setuped */
				/* Send event to IP */
				x_branch_pvt_message x_br_pvt_msg;

				create_pvt_msg (&x_br_pvt_msg, E_PVT_MSG_TYPE_EVENT,
								E_PVT_EVT_SWITCH_UNCONFIGURED, NULL, 
								0, NULL,
								E_QUEUE_MAIN,
								E_QUEUE_IP_MSG);

				ret_val = br_send_msg (E_QUEUE_IP_MSG, &x_br_pvt_msg, NULL, 0);
			}
		}
		break;

		default:
        {
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt = E_PVT_MSG_NONE;
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }
LABEL_RETURN:
	return (ret_val);
}  /* handle_standby_event_msg () */

/* EOF branch_state_standby.c */
