/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
File name   : branch_state_setup.c
Author      : RAHUL NAKHATE
Description : This file contains implementation for handling setup state
			  private messaging
*******************************************************************************/

/******************************* I N C L U D E S ******************************/

#include "branch_types.h"
#include "branch_utils.h"
#include "branch_debug_print.h"
#include "branch_pvt_messages.h"
#include "branch_device_info.h"
#include "branch_state_private.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

#define VIDEO_DETECT_TIME_OUT			(20 * 1000)  /* Sec */
#define DEVICE_INFO_TIME_OUT			(1 * 1000)  /* Sec */

#define MAX_DEVICE_INFO_RETRY_COUNT		(10)

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

static bool_t video_detect_time_out_state = false;

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/

static int8_t
handle_setup_req_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					  x_branch_pvt_message *px_resp_pvt_msg);
static int8_t
handle_setup_resp_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					   x_branch_pvt_message *px_resp_pvt_msg);
static int8_t
handle_setup_event_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					    x_branch_pvt_message *px_resp_pvt_msg);

static uint8_t*
get_curr_hdmi_port (void);

static int8_t
handle_get_device_info_resp (x_branch_pvt_message *px_rcvd_pvt_msg,
							 x_branch_pvt_message *px_resp_pvt_msg);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

int8_t parse_setup_state_pvt_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
								  x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}

	switch (px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type)
    {
		case E_PVT_MSG_TYPE_REQUEST:
        {
            ret_val = handle_setup_req_msg (px_rcvd_pvt_msg, px_resp_pvt_msg);
        }
        break;
		case E_PVT_MSG_TYPE_RESPONSE:
        {
            ret_val = handle_setup_resp_msg (px_rcvd_pvt_msg, px_resp_pvt_msg);
        }
        break;
		case E_PVT_MSG_TYPE_EVENT:
        {
            ret_val = handle_setup_event_msg (px_rcvd_pvt_msg, px_resp_pvt_msg);
        }
        break;

        default:
        {
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }

LABEL_RETURN:
	return (ret_val);
}  /* parse_setup_state_pvt_msg () */

void
set_video_detect_time_out_state (bool_t flag)
{
	video_detect_time_out_state = flag;
}  /* set_video_detect_time_out_state () */

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/

static int8_t
handle_setup_req_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					  x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}

	px_resp_pvt_msg->x_pvt_msg_hdr.e_source = E_QUEUE_MAIN;
	px_resp_pvt_msg->x_pvt_msg_hdr.e_destination =
								px_rcvd_pvt_msg->x_pvt_msg_hdr.e_source;
	px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt =
								px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt;

	switch (px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt)
    {
		case E_PVT_MSG_GET_DEVICE_INFO:
		{
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type =
											E_PVT_MSG_TYPE_RESPONSE;
			get_device_info ((x_hdmi_port_info *)px_resp_pvt_msg->payload,
						px_resp_pvt_msg->x_pvt_msg_hdr.min_payload[0]);
			px_resp_pvt_msg->x_pvt_msg_hdr.payload_size =
								sizeof (x_hdmi_port_info);
		}
		break;
		case E_PVT_MSG_SET_DEVICE_ID:
        {
			x_branch_pvt_message x_send_msg;
			x_hdmi_port_info x_device_info_list[NO_OF_HDMI_PORTS];
			uint8_t *p_curr_hdmi_port = NULL;
			uint8_t remove_device = false;

			/* Get the current port */
			p_curr_hdmi_port = get_curr_hdmi_port ();

			/* Update the device ID */
			get_device_info_list (x_device_info_list);
			x_device_info_list[*p_curr_hdmi_port].device_id =
						px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[0];
			if (0x0000 == x_device_info_list[*p_curr_hdmi_port].device_id)
			{
				remove_device = true;
			}
			set_device_info (&x_device_info_list[*p_curr_hdmi_port],
							 (*p_curr_hdmi_port),
							 remove_device);

			/* Send the response */
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type =
												E_PVT_MSG_TYPE_RESPONSE;
			px_resp_pvt_msg->x_pvt_msg_hdr.min_payload[0] = 0;
		
			/* Send stop video detect */
			ret_val = create_pvt_msg (&x_send_msg,
										E_PVT_MSG_TYPE_REQUEST,
										E_PVT_MSG_STOP_VIDEO_DETECT,
										NULL,
										0,
										NULL,
										E_QUEUE_MAIN,
										E_QUEUE_VIDEO_DETECT_MSG);
			
			ret_val = (int8_t) br_send_msg (
									x_send_msg.x_pvt_msg_hdr.e_destination,
								    &x_send_msg, NULL, 0);
        }
        break;
		case E_PVT_MSG_GET_DEVICE_INFO_LIST:
		{
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type =
											E_PVT_MSG_TYPE_RESPONSE;
			get_device_info_list ((x_hdmi_port_info *)
									px_resp_pvt_msg->payload);
			px_resp_pvt_msg->x_pvt_msg_hdr.payload_size =
								sizeof (x_hdmi_port_info) * NO_OF_HDMI_PORTS;
		}
		break;

		default:
        {
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt = E_PVT_MSG_NONE;
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }

LABEL_RETURN:
	return (ret_val);
}  /* handle_setup_req_msg () */

static int8_t
handle_setup_resp_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					   x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}

	switch (px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt)
    {
		case E_PVT_MSG_GET_DEVICE_INFO:
		{
			ret_val = handle_get_device_info_resp (px_rcvd_pvt_msg,
												   px_resp_pvt_msg);
		}
		break;
		case E_PVT_MSG_STOP_VIDEO_DETECT:
		{
			if (true == video_detect_time_out_state)
			{
				/* Time out was occured */
				set_video_detect_time_out_state (false);
			}
			else
			{
				/* Send the updated device info list to Neo */
				px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_type =
												E_PVT_MSG_TYPE_REQUEST;
				px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt =
												E_PVT_MSG_SET_DEVICE_INFO_LIST;
				get_device_info_list ((x_hdmi_port_info *)
									  px_resp_pvt_msg->payload);
				px_resp_pvt_msg->x_pvt_msg_hdr.payload_size =
					sizeof (x_hdmi_port_info) * NO_OF_HDMI_PORTS;

				px_resp_pvt_msg->x_pvt_msg_hdr.e_source = E_QUEUE_MAIN;
				px_resp_pvt_msg->x_pvt_msg_hdr.e_destination = E_QUEUE_BT_MSG;
			}
		}
		break;
		case E_PVT_MSG_SET_DEVICE_INFO_LIST:
        {
			if (E_QUEUE_BT_MSG == px_rcvd_pvt_msg->x_pvt_msg_hdr.e_source)
			{
				/* Change state to active state */
				br_set_state (E_SWITCH_STATE_ACTIVE);
			}
        }
        break;

		default:
        {
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }

LABEL_RETURN:
	return (ret_val);
}  /* handle_setup_resp_msg () */

static int8_t
handle_setup_event_msg (x_branch_pvt_message *px_rcvd_pvt_msg,
					    x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;

	if ((NULL == px_rcvd_pvt_msg) ||
		(NULL == px_resp_pvt_msg))
	{
		ret_val = -1;
		goto LABEL_RETURN;
	}
	
	switch (px_rcvd_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt)
    {
		case E_PVT_EVT_BT_DISCONNECTED:
		{
			/* Goto standby */
			x_switch_context *px_context = NULL;

			ret_val = br_get_context (&px_context);
			if (NULL == px_context)
			{
				ret_val = -1;
				goto LABEL_RETURN;
			}
			px_context->e_state = E_SWITCH_STATE_STANDBY;
		}
		break;

		case E_PVT_EVT_VIDEO_DETECTED:
        {
			/* forward to IP thread */
			uint32_t device_id = 0;

			/* Stop the timer if running */
			br_stop_timer ();
			
			device_id = ((px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[0]
						  & 0x0000FFFF) << 16);
			device_id |= (px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[1]
						  & 0x0000FFFF);
			px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[0] = device_id;
			px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[1] = 0;

			memcpy (px_resp_pvt_msg,
				    px_rcvd_pvt_msg,
					get_pvt_msg_size (px_rcvd_pvt_msg));
			px_resp_pvt_msg->x_pvt_msg_hdr.e_pvt_msg_evt =
										E_PVT_EVT_SOURCE_IDENTIFIED;
            px_resp_pvt_msg->x_pvt_msg_hdr.e_source      = E_QUEUE_MAIN;
			px_resp_pvt_msg->x_pvt_msg_hdr.e_destination = E_QUEUE_IP_MSG;
        }
        break;
		
		default:
        {
            ret_val = -2;
            goto LABEL_RETURN;
        }
    }
LABEL_RETURN:
	return (ret_val);
}  /* handle_setup_event_msg () */

static uint8_t*
get_curr_hdmi_port (void)
{
	x_switch_context *px_context = NULL;
	uint8_t *p_curr_port = NULL;

	br_get_context (&px_context);
	if (NULL == px_context)
	{
		goto LABEL_RETURN;
	}
	p_curr_port = &px_context->curr_hdmi_setup_port;

LABEL_RETURN:
	return (p_curr_port);
}  /* get_curr_hdmi_port () */


static int8_t
handle_get_device_info_resp (x_branch_pvt_message *px_rcvd_pvt_msg,
							 x_branch_pvt_message *px_resp_pvt_msg)
{
	int8_t ret_val = 0;
	x_hdmi_port_info *px_hdmi_device_info = NULL;
	uint8_t *p_curr_hdmi_port = NULL;

	/* Check the resp */
	if (NO_ERROR == px_rcvd_pvt_msg->x_pvt_msg_hdr.min_payload[0])
	{
		/* Pass response */
		p_curr_hdmi_port = get_curr_hdmi_port ();
		if (NULL == p_curr_hdmi_port)
		{
			ret_val = -1;
			goto LABEL_RETURN;
		}

		px_hdmi_device_info = (x_hdmi_port_info *)px_rcvd_pvt_msg->payload;
		/* Update local device info */
		set_device_info (px_hdmi_device_info, (*p_curr_hdmi_port), false);

		/* If device is NOT detected via HDMI-CEC */
		px_hdmi_device_info->device_id = 0;
		if (0 == px_hdmi_device_info->device_id)
		{
			/* Send start video detect */
			debug_printf (E_DEBUG_LEVEL_INFO,
						  "Sending start video detect");
			ret_val = create_pvt_msg (px_resp_pvt_msg,
										E_PVT_MSG_TYPE_REQUEST,
										E_PVT_MSG_START_VIDEO_DETECT,
										NULL,
										0,
										NULL,
										E_QUEUE_MAIN,
										E_QUEUE_VIDEO_DETECT_MSG);
			/* Set timer */
			br_start_timer (VIDEO_DETECT_TIME_OUT,
							E_TIMER_STATE_VIDEO_DETECT);
		}
		else
		{
			uint32_t min_payload[BRANCH_PVT_MIN_PAYLOAD_SIZE] = {0, 0};

			debug_printf (E_DEBUG_LEVEL_INFO,
						  "Device is detected by CEC, sending detected event");
			/* Send device detected event */
			min_payload[0] = px_hdmi_device_info->device_id;
			ret_val = create_pvt_msg (px_resp_pvt_msg,
										E_PVT_MSG_TYPE_EVENT,
										E_PVT_EVT_SOURCE_IDENTIFIED,
										min_payload,
										0,
										NULL,
										E_QUEUE_MAIN,
										E_QUEUE_IP_MSG);
		}
	}
	else
	{
		/* Failed response,
		   User might not have turn on the device or
		   device might not hav given 5v input,
		   wait for some time and retry */
		/* Increment retry count */
		x_switch_context *px_switch_contex;

		ret_val = br_get_context (&px_switch_contex);
		if (NULL == px_switch_contex)
		{
			goto LABEL_RETURN;
		}
		px_switch_contex->get_device_info_retry_count ++;

		if (MAX_DEVICE_INFO_RETRY_COUNT ==
						px_switch_contex->get_device_info_retry_count)
		{
			uint32_t min_payload[BRANCH_PVT_MIN_PAYLOAD_SIZE] = {0, 0};

			debug_printf (E_DEBUG_LEVEL_INFO,
						  "Max retry for get info, sending no device event");
			/* Max retry */
			px_switch_contex->get_device_info_retry_count = 0;
			/* Send failure msg to phone */
			min_payload[0] = 0xFFFFFFFF;
			ret_val = create_pvt_msg (px_resp_pvt_msg,
									  E_PVT_MSG_TYPE_EVENT,
									  E_PVT_EVT_SOURCE_IDENTIFIED,
									  min_payload,
									  0,
									  NULL,
									  E_QUEUE_MAIN,
									  E_QUEUE_IP_MSG);
			/* Exit setup */
			br_set_state (E_SWITCH_STATE_ACTIVE);
		}
		else
		{
			/* Wait for some time and ask again for device info */
			debug_printf (E_DEBUG_LEVEL_INFO,
						  "Device is NOT connected, waiting for %d ms",
							DEVICE_INFO_TIME_OUT);

			br_start_timer (DEVICE_INFO_TIME_OUT,
							E_TIMER_STATE_GET_DEVICE_INFO);
		}
	}

LABEL_RETURN:
	return (ret_val);
}  /* handle_get_device_info_resp () */

/* EOF branch_state_setup.c */
