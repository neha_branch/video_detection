/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
* File name   :     TemplateMatching.h
*
* Author      :     Neha Mittal
*
* Description :     Template Matching process 
*
*******************************************************************************/
#ifndef _TEMPLATE_MATCHING_H
#define _TEMPLATE_MATCHING_H

/******************************* I N C L U D E S ******************************/
#include "VideoDB.h"
#include "ImageClass.h"

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/
struct s_db_tm_data
{
    int img_size;   /*size in bytes*/
    unsigned char* img;
};
struct s_db_tm_metadata
{
    //int img_size; /*size in bytes*/
    int width;
    int height;
    int planes;
};
struct TemplateInfo
{
    string devicename;
    Mat img_template;
    int nMatches;       /*Number of consecutive matches required.*/
    string f_name;
    int deviceIndex;
};

/****************************** F U N C T I O N S *****************************/

int initTemplateMatching();
int TemplateMatching(Mat frame, vidDetectPayload *result);

#endif /*_TEMPLATE_MATCHING_H*/
