/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : TemplateMatching.cpp
Author       : Neha Mittal
Description  : This file contains function definition for Template Matching.
*******************************************************************************/

/******************************* I N C L U D E S ******************************/
#include "TemplateMatching.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/
//#define DEBUG_TM

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/
vector<TemplateInfo> TEMPLATES;

/********************* S T A T I C   V A R I A B L E S ************************/

/********* P R I V A T E   F U N C T I O N   D E C L A R A T I O N S **********/
int MatchTemplate(Mat img, Mat templ, double* retParam);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/
int initTemplateMatching()
{
    vector<videoDB> dB;
	int dBlen = 0;
    dBlen = AccessVideoDB (dB, "./db/templates", "jpg");
	if( dBlen == 0 )
	{
		debug_printf(E_DEBUG_LEVEL_CRITICAL_ERROR, "Template Database empty");
	}

    for(int i = 0; i < (int)dB.size(); i++)
    {
        /* Read each image in the database,
           and save in a global variable! */
        Mat image;
        TemplateInfo ti;
        
        image = imread(dB[i].dB_filename);//, CV_LOAD_IMAGE_GRAYSCALE );
        ti.img_template = image;
        ti.devicename = dB[i].device_name;
        ti.deviceIndex = get_device_id( (char*)dB[i].device_name.c_str() );
        ti.f_name = dB[i].dB_filename;
        if( ti.deviceIndex > NO_DEVICE )
        {
            TEMPLATES.push_back(ti);
        }
        else
        {
            /*device not listed in the deviceid enum */
        }
    }
    debug_printf(E_DEBUG_LEVEL_INFO, "Template matching has %d templates", 
                                                             TEMPLATES.size() );
    return 0;
}

int TemplateMatching(Mat frame, vidDetectPayload* result)
{
    int devFound = 0;
    double matchParam = 0;
    char filename[100];
    int device_id = -1;
    int ret = 0;

    s_db_tm_data template_data;
    TemplateInfo TEMPLATE;
    Mat image;

    result->sDetectInfo.detScheme = eTEMPLATE_MATCHING;
    /* If the frame is a single color frame, identification is not possible */
    if( isSingleColorImage( frame, 99))
    {
        return(0);
    }
    if( frame.channels() != 3 )
    {
        debug_printf( E_DEBUG_LEVEL_ERROR, 
           "TemplateMatching: image doesn't have three channels, returning\n" );
    }
    //for( int nTempl = 0; nTempl < (int)TEMPLATES.size(); nTempl++)
    while(1)
    {
    	ret = get_db_data( eTEMPLATE_MATCHING, &device_id, (void*) &template_data);
    	if ( 1 == ret )
		{
			break;
		}
    	std::vector<char> data(template_data.img, template_data.img+template_data.img_size);
    	image = imdecode( Mat(data), CV_LOAD_IMAGE_COLOR );

    	//imwrite("test_2.jpg", image);
    	//image = imread(filename);//, CV_LOAD_IMAGE_GRAYSCALE );
    	TEMPLATE.img_template = image;
		//ti.devicename = dB[i].device_name;
		//ti.deviceIndex = get_device_id( (char*)dB[i].device_name.c_str() );
		//TEMPLATE.f_name = dB[i].dB_filename;
        int matches = 0;
        /*if( devFound == TEMPLATES[nTempl].deviceIndex ) 
        {
            continue;
        }*/
        
        matches = MatchTemplate( frame, 
                                 TEMPLATE.img_template,
                                 &matchParam );
#ifdef DEBUG_TM
            cout<<endl<<"TEMPLATE MATCH "<< TEMPLATES[nTempl].devicename<<" "\
                <<TEMPLATES[nTempl].f_name<<"\t"<<matchParam;
#endif
        if( matches )
        {           
        	printf("\n TEMPLATE MATCH FOUND device_id:%d", device_id);
            /*devFound = TEMPLATES[nTempl].deviceIndex;
            result->sDeviceIdInfo.devModel = TEMPLATES[nTempl].deviceIndex;
            result->sDetectInfo.perfectMatch = 1;
            result->sDetectInfo.measureVal = matchParam;*/
#ifdef DEBUG_TM
            cout<<endl<<"TEMPLATE MATCH "<< TEMPLATES[nTempl].devicename<<" "\
                <<TEMPLATES[nTempl].f_name;
#endif
            reset_db_counter( eTEMPLATE_MATCHING );
            break;
        }
    }
    return devFound;
}

/********** P R I V A T E   F U N C T I O N   D E F I N I T I O N S ***********/
int MatchTemplate(Mat img, Mat templ, double* matchParam)
{
    Mat result;
    Mat mean, Var;
    int match_method = CV_TM_CCORR_NORMED;//CV_TM_CCOEFF_NORMED;
    //int threshold = 7;
    //float sqdiff_thres = 0.2f;
    float ccorr_thres  = 0.99f;
    double minVal; double maxVal; Point minLoc; Point maxLoc;
    Point matchLoc;
    //double mn = 0.0, sd = 0.0;

    int result_cols =  img.cols - templ.cols + 1;
    int result_rows = img.rows - templ.rows + 1;

    if ( (result_cols < 1) || (result_rows < 1) )
    {
        return(0);
    }
    result.create( result_cols, result_rows, CV_32FC1 );

    /* OpenCV function call for template matching */
    matchTemplate( img, templ, result, match_method );
    //normalize( result, result, 0, 1, NORM_MINMAX, -1, Mat() );    
    
    //cv::meanStdDev(result, mean, Var);   
    minMaxLoc( result, &minVal, &maxVal, &minLoc, &maxLoc, Mat() );
    /* 
       For SQDIFF and SQDIFF_NORMED, the best matches are lower values. 
       For all the other methods, the higher the better
    */
    if( match_method  == CV_TM_SQDIFF || match_method == CV_TM_SQDIFF_NORMED )
    { 
        matchLoc = minLoc; 
    }
    else
    {
        matchLoc = maxLoc; 
    }
        
    //mn = mean.at<double>(0,0);
    //sd = Var.at<double>(0,0);
    
    //if( maxVal > mn+threshold*sd )
    {
        /* Extract the detected template and 
           double check with the template using square differnce 
        */
#ifdef DEBUG_TM
        Mat F = img.clone();
        Mat D(F, Rect(matchLoc.x, matchLoc.y, templ.cols , templ.rows));
#endif
        //result_cols =  D.cols - templ.cols + 1;
        //result_rows = D.rows - templ.rows + 1;
        //result.create( result_cols, result_rows, CV_32FC1 );

        //matchTemplate( D, templ, result, CV_TM_CCORR_NORMED/*CV_TM_SQDIFF_NORMED*/ );        
       
        //if( result.at<float>(0) <= sqdiff_thres )
        *matchParam = maxVal;
        if( maxVal > ccorr_thres )        
        {
            *matchParam = maxVal;
#ifdef DEBUG_TM
            cout<< endl<< result;            
            imshow("Found Template", D);      
#endif
            return (1); 
        }
        else
        {
            return (0); 
        }
    }    
    return 0;
}
