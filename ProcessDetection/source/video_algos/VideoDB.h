/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
* File name   :     VideoDB.h
*
* Author      :     Neha Mittal
*
* Description :     API for acceccing database process 
*
*******************************************************************************/
#ifndef _VIDEO_DB_H
#define _VIDEO_DB_H

/******************************* I N C L U D E S ******************************/
#ifdef WIN32
#include <windows.h>
#endif
#include <iostream>
#include <string>
#include <vector>

#include "common_header.h"

using namespace std;

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/
struct videoDB
{
    string device_name;//folder_name
    string dB_filename;
    string mdata1;
    int prop1;
    int prop2;
};

/****************************** F U N C T I O N S *****************************/
int AccessVideoDB (vector<videoDB>& db, string baseFolder, string type);

int get_db_data( int scheme, int *dev_id, void* db_entry);

void reset_db_counter( int scheme );

#endif /* _VIDEO_DB_H */
