/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
* File name   :     LogoDetection.h
*
* Author      :     Neha Mittal
*
* Description :     Logo Detection process 
*
*******************************************************************************/
#ifndef _LOGO_DETECTION_H
#define _LOGO_DETECTION_H

/******************************* I N C L U D E S ******************************/
#include "VideoDB.h"
#include "ImageClass.h"

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/
class FeatureArray
{
public:
    FeatureArray()
    {

    }
    /*Constructor*/
    FeatureArray(Mat img)
    {
        image = img;
        keypoints.reserve(500);
        _getRawORBKeypoints(img, 500, keypoints, descriptor, false);

        nFeatures = (int)keypoints.size();
    }
    FeatureArray(Mat img, int nFeat)
    {
        image = img;
        keypoints.reserve(500);
        _getRawORBKeypoints(img, nFeat, keypoints, descriptor, false);
        nFeatures = (int)keypoints.size();
    }
    Mat image;
    vector<KeyPoint> keypoints;
    Mat descriptor;
    int nFeatures;
};

typedef struct s_db_ld_data
{
    int num_kps;
    Point *kp_add;
    unsigned char* desc_add;
};

struct LogoInfo
{
    FeatureArray features;
    string logoname;
    string addnLogoCheck;
    int nFeatMatch;         /*Number of features to match to call it a perfect match.*/
    int nMatches;           /*Number of consecutive matches required.*/
    int deviceIndex;        /*Device identifier */
    int deviceVerIndex;
};

/****************************** F U N C T I O N S *****************************/
int initLogoDetection();
int LogoDetection(Mat frame, vidDetectPayload* result);
#endif /* _LOGO_DETECTION_H */
