/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : VideoDB.cpp
Author       : Neha Mittal
Description  : This file contains function definition for Logo Detection.
*******************************************************************************/

/******************************* I N C L U D E S ******************************/
#include <stdio.h>
#include <string.h>
#include <algorithm>
#ifdef WIN32
#include <io.h>
#else
#include <dirent.h>
#endif
#include "VideoDB.h"
#include "access_dB.h"

#include "LogoDetection.h"
#include "TemplateMatching.h"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/
int findMetaData( string dir, char*f_name, char* info);
int readdata_from_json_obj(json_object* json, int scheme, void* data);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/
int AccessVideoDB(vector<videoDB>& database, string baseFolder, string type)
{
    vector<string> device_names;
    DIR *d;
    //struct _finddata_t c_file;
    //long hFile;
    database.clear();
   
    string buffer = baseFolder+"\\*";

    /* Open Directory */
    d = opendir(baseFolder.c_str());

    /* Check if directory was opened */
    if( !d )
    {
    	debug_printf(E_DEBUG_LEVEL_INFO, "Can't open directory: %s",baseFolder.c_str());
    }
    //hFile = _findfirst( buffer.c_str(), &c_file );
    /*Check to make sure that there are files in directory*/
    //if( !d )
    {
    	struct dirent * entry;
    	//const char * d_name;
        while(1)
        {
        	entry = readdir(d);
        	if( !entry )
        	{
        		break;
        	}
            if(entry->d_type & DT_DIR)
            {

                /* sub-directory */
                if ( (strcmp(entry->d_name, ".") == 0) ||
                	 (strcmp(entry->d_name, "..") == 0) )
                {
                    continue;
                }
                //printf("\n%s",entry->d_name);
                device_names.push_back(entry->d_name);

            }      
        };
        // Close file finder object
        //_findclose( hFile );
        closedir(d);
    }
    /* Extract saved logos */
    //printf( "Listing of files:\n" );
    // if there are no sub folders, return the files in the current folder
//    if( device_names.size() == 0 )
//    {
//        videoDB dB;
//
//        buffer = baseFolder+"\\*."+type;
//        string currDir = baseFolder+"\\";
//
//        hFile = _findfirst( buffer.c_str(), &c_file );
//        if( hFile != -1L )
//        {
//            do
//            {
//                string s = string(c_file.name);
//                string delimiter = ".";
//                string token = s.substr(s.find(delimiter)+1, 3);
//                if(_strcmpi(token.c_str(), type.c_str()))
//                    continue;
//                string filename = currDir+string(c_file.name);
//                dB.device_name = "";
//                dB.dB_filename = filename;
//                dB.prop1 = 0;
//                dB.prop2 = 0;
//                database.push_back(dB);
//            }while( _findnext( hFile, &c_file ) == 0 );
//        }
//        return ( (int)database.size() ); /* TODO: replace with goto */
//    }

    for(int i = 0; i < (int)device_names.size(); i++)
    {
        videoDB dB;
        //buffer = baseFolder+"\\"+device_names[i]+"\\*."+type;
        //string currDir = baseFolder+"\\"+device_names[i]+"\\";
        buffer = baseFolder+"/"+device_names[i]+"/*."+type;
        string currDir = baseFolder+"/"+device_names[i]+"/";

        //printf("\n%s",currDir.c_str());
        d = opendir(currDir.c_str());

        //hFile = _findfirst( buffer.c_str(), &c_file );
        if( d )
        {
        	struct dirent * entry;
        	//const char * d_name;
        	while(1)
        	{
        		char mdata[50];
                string s = "";//string(entry->d_name);
                string delimiter = ".";
                string token = "";//s.substr(s.find(delimiter)+1, 3);

                entry = readdir(d);
				if( !entry )
				{
					break;
				}
				s = string(entry->d_name);
				token = s.substr(s.find(delimiter)+1, 3);
				std::transform(token.begin(), token.end(), token.begin(), ::tolower);

                if ( (strcmp(entry->d_name, ".") == 0) ||
					 (strcmp(entry->d_name, "..") == 0) )
				{
					continue;
				}
                //printf("\n\t%s",entry->d_name);

                if(strcmp(token.c_str(), type.c_str()))
                    continue;
                string filename = currDir+string(entry->d_name);

                if (findMetaData(currDir, entry->d_name, mdata))
                {
                    dB.mdata1 = string(mdata);
                }
                else
                {
                    dB.mdata1 = "";
                }
                dB.device_name = device_names[i];
                dB.dB_filename = filename;
                dB.prop1 = 0;
                dB.prop2 = 0;
                database.push_back(dB);
                //printf(" ");
            };//while( _findnext( hFile, &c_file ) == 0 );
        }
    }
    return ( (int)database.size() );
    //return 0;
}

int get_db_data( int scheme, int *dev_id, void* db_entry)
{
    json_object *json = NULL;
    int ret_val = 0;
    /*call http query*/
    if( eFINGERPRINTING == scheme )
    {
        /*search data for data*/
        ret_val = search_for_match(scheme, dev_id, db_entry);
        if( 0 == ret_val )
        	debug_printf(E_DEBUG_LEVEL_DEBUG, "FP: Match found, dev_id: %d", *dev_id);
    }
    else
    {
        //ret_val = get_data_from_file(dev_id, db_entry);
        ret_val = get_next_db_entry( scheme, dev_id, &json);
        readdata_from_json_obj(json, scheme, db_entry);
    }

    return (ret_val);
}

void reset_db_counter( int scheme )
{
    reset_db_access_pointer( scheme );

}

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/
int readdata_from_json_obj(json_object* json, int scheme, void* data)
{
    //  = (s_db_ld_data*)malloc(sizeof(s_db_ld_data));
    json_object *json_temp       = NULL;
    json_object *j_kp       = NULL;
    json_object *j_desc     = NULL;
    int data_size = 0;

    FILE *fptr = NULL;
	char rec_file_name[50] ;
	static int file_num = 0;
	unsigned int temp_int = 0;
	//int data_size = 0;

    int ret_val = 0;
    int n = 0, j = 0;

    if( NULL == data )
    {
        ret_val = -1;
        goto LABEL_RETURN;
    }

    char filename[100];

    switch(scheme)
    {
    case eLOGO_DETECTION:
    {
        s_db_ld_data* db_entry;
    	db_entry = (s_db_ld_data*)data;
        json_temp           = json_object_object_get(json, "metadata");
        db_entry->num_kps   = json_object_get_int(json_temp);

        j_kp           = json_object_object_get(json, "kps");
        //j_kp = json_object_new_array ();
        db_entry->kp_add = (Point*)malloc(db_entry->num_kps * sizeof(Point));
        for( n = 0; n < db_entry->num_kps; n++)
        {
            json_object *json_temp       = NULL;
            json_temp                   = json_object_array_get_idx(j_kp, 2*n);
            db_entry->kp_add[n].x       = json_object_get_int(json_temp);
            json_temp                   = json_object_array_get_idx(j_kp, 2*n+1);
            db_entry->kp_add[n].y       = json_object_get_int(json_temp);
        }
        //json_object_object_add (json, "kps", j_kp);

        j_desc           = json_object_object_get(json, "desc");
        //j_desc = json_object_new_array ();
        db_entry->desc_add = (unsigned char*)malloc(db_entry->num_kps * 32*sizeof(char));
        memset( db_entry->desc_add, 0, db_entry->num_kps * 32*sizeof(char));

        j = 0;
        for( n = 0; n < db_entry->num_kps*8; n++)
        {
            //for (j = 0; j < 32; j+=4)
            {
                json_object *json_temp       = NULL;
                json_temp = json_object_array_get_idx(j_desc, j);
                //temp_int = json_object_get_type(json_temp);
                temp_int = (unsigned int)json_object_get_int(json_temp);
                memcpy(((int*)db_entry->desc_add + j) , &temp_int, sizeof(int));
                //*(db_entry->desc_add + (n*32+j)) = (unsigned int)json_object_get_int(json_temp);
                j++;
            }
        }
        data_size = sizeof(int) + (db_entry->num_kps) * sizeof(Point) + (db_entry->num_kps)*32*sizeof(char);
        sprintf(rec_file_name,"./recovered/%d.txt",file_num++);

        fptr = fopen(rec_file_name, "wb");
        fwrite((void*)&db_entry->num_kps, sizeof(int), 1, fptr);
        fwrite((void*)db_entry->kp_add, (db_entry->num_kps) * sizeof(Point), 1, fptr);
        fwrite((void*)db_entry->desc_add, (db_entry->num_kps) * 32 * sizeof(char), 1, fptr);

        fclose(fptr);
    }
	break;

    case eTEMPLATE_MATCHING:
        s_db_tm_data *db_entry;
        int len = 0;
        db_entry = (s_db_tm_data*)data;
        json_temp           = json_object_object_get(json, "size");
        db_entry->img_size  = json_object_get_int(json_temp);

        len = ceil( (float)db_entry->img_size/sizeof(int) );
        j_desc           = json_object_object_get(json, "img");
        //j_desc = json_object_new_array ();
        db_entry->img = (unsigned char*)malloc(db_entry->img_size *sizeof(char));
        memset( db_entry->img, 0, db_entry->img_size * sizeof(char));

        j = 0;
        for( n = 0; n < len; n++)
        {
            json_object *json_temp       = NULL;
            json_temp = json_object_array_get_idx(j_desc, j);
            //temp_int = json_object_get_type(json_temp);
            temp_int = (unsigned int)json_object_get_int(json_temp);
            memcpy(((int*)db_entry->img + j) , &temp_int, sizeof(int));
            //*(db_entry->desc_add + (n*32+j)) = (unsigned int)json_object_get_int(json_temp);
            j++;
        }
    	break;
    }
    LABEL_RETURN:
    return (data_size);
}
int findMetaData( string dir, char*f_name, char* info)
{
    int found = 0;
//    string metadata_filename = dir+string (strtok(f_name, "."))+ ".txt";;
//    FILE *temp_fptr = NULL;
//
//    temp_fptr = fopen( metadata_filename.c_str(), "r" );
//    if( temp_fptr != NULL )
//    {
//        found = 1;
//        fscanf( temp_fptr, "%s", info );
//        fclose ( temp_fptr );
//        temp_fptr = NULL;
//    }
    return found;
}
