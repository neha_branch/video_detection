/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : LogoDetection.cpp
Author       : Neha Mittal
Description  : This file contains function definition for Logo Detection.
*******************************************************************************/

/******************************* I N C L U D E S ******************************/
#include "LogoDetection.h"
#include "opencv2/calib3d/calib3d.hpp"

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/
#define MATCHING_TH (20)
#define NUM_FEATURES_PER_FRAME (2000)
//#define DEBUG_LD

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/
vector<LogoInfo> LOGOS;
//extern s_db_ld_data *db_entry;

/********************* S T A T I C   V A R I A B L E S ************************/

/********* P R I V A T E   F U N C T I O N   D E C L A R A T I O N S **********/
int findMatches( FeatureArray& frame, s_db_ld_data& logo );
vector<DMatch> filterMatches( vector<vector<DMatch> >& matches,
                              Point* kp1,
							  vector<KeyPoint>& kp2,
							  float ratio,
							  vector<Point2f>& p1,
							  vector<Point2f>& p2);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/
/* Read logos from database and store the keypoints and descriptors */
int initLogoDetection()
{
    vector<videoDB> dB;
	int dBlen = 0;
    dBlen = AccessVideoDB(dB, "./db/logos", "jpg");
	if( dBlen == 0 )
	{
		debug_printf(E_DEBUG_LEVEL_CRITICAL_ERROR, "Logo Database empty");
	}

    for(int i = 0; i < (int)dB.size(); i++)
    {
        /* read the image at each location,
        extract features and save in a global variable!*/
        LogoInfo li;
        Mat image;
        
        image = imread(dB[i].dB_filename, CV_LOAD_IMAGE_GRAYSCALE );
        li.features = FeatureArray(image,300);
        li.logoname = dB[i].device_name;
        li.addnLogoCheck = dB[i].mdata1;

        int pos = dB[i].device_name.find("_");
        if( pos != -1 )
        {
            string devName = dB[i].device_name.substr(0,pos);
            string ver = dB[i].device_name.substr(pos+1);

            li.deviceIndex = get_device_id( (char*)devName.c_str() );
            li.deviceVerIndex = get_device_version( (char*)ver.c_str() );
        }
        else
        {
            li.deviceIndex = get_device_id( (char*)dB[i].device_name.c_str() );
            li.deviceVerIndex = NO_VERSION;
        }
        
        if( li.deviceIndex > NO_DEVICE )
        {
            LOGOS.push_back(li);
        }
        else
        {
            /*device not listed in the deviceid enum */
        }              
    }
    debug_printf(E_DEBUG_LEVEL_INFO, "Logo detection database has %d logos",
                                                                 LOGOS.size() );
    return 0;
}

int LogoDetection(Mat frame, vidDetectPayload* result)
{
    FeatureArray targFrame(frame, NUM_FEATURES_PER_FRAME);
    string checkXtraDevName = "";

    s_db_ld_data LOGO;
    int device_id = -1;
    int devFound = -1;
	int matches = 0, ret = 0;

    result->sDetectInfo.detScheme = eLOGO_DETECTION;

    if( targFrame.nFeatures < MATCHING_TH )
    {
        return(-1);
    }

    /* read database */
    while ( 1 )
    {
        ret = get_db_data( eLOGO_DETECTION, &device_id, (void*) &LOGO);
        if ( 1 == ret )
        {
            break;
        }
        /* Iterate through all the logos to find a match */
        //for( int nLogo= 0; nLogo < (int)LOGOS.size(); nLogo++)
        {
            /*if( ( devFound > -1 ) && (checkXtraDevName != LOGOS[nLogo].logoname) )
            {
                continue;
            }*/
            matches = findMatches(targFrame, LOGO);
            if( matches >= MATCHING_TH )
            {
            	printf(" \n MATCH FOUND FOR DEVICE id: %d num_matches= %d", device_id, matches);
                /*devFound = LOGOS[nLogo].deviceIndex;
                result->sDeviceIdInfo.devModel = LOGOS[nLogo].deviceIndex;
                result->sDeviceIdInfo.devVer   = LOGOS[nLogo].deviceVerIndex;
                result->sDetectInfo.perfectMatch = 1;
                result->sDetectInfo.measureVal = matches;*/
                /*if( LOGOS[nLogo].addnLogoCheck != "" )
                {
                    checkXtraDevName = LOGOS[nLogo].addnLogoCheck;
                    //strcpy( (char*)checkXtraDevName.c_str(), LOGOS[nLogo].addnLogoCheck.c_str() );
                }
                else
                {
                    #ifdef DEBUG_LD
                    cout<<endl<<"LOGO "<< LOGOS[nLogo].logoname;
                    cout<< "num of matches found for "<< LOGOS[nLogo].logoname<<" = " \
                        << matches<<" "<< nLogo<< endl;
                    #endif
                    break;
                }*/
            	reset_db_counter( eLOGO_DETECTION );
            	break;
            }
            //printf("\n Number of matches: %d",matches);
        }

        /* Free the logo memory*/


        free(LOGO.kp_add);
        LOGO.kp_add = NULL;

        free(LOGO.desc_add);
        LOGO.desc_add = NULL;
    }
    //free(&LOGO);
    //db_entry = NULL;

    return(devFound);
}

/********** P R I V A T E   F U N C T I O N   D E F I N I T I O N S ***********/
/* Match the features of the given frame with the logo coming from the database
   Uses FLANN based matching
   Returns the number of inliners
*/
int findMatches(FeatureArray& frame, s_db_ld_data& logo)
{
    FlannBasedMatcher matcher(new flann::LshIndexParams(6,12,1));
    vector<vector<DMatch> > matches;
    vector<DMatch> goodmatches;
    vector<Point2f> p1,p2;
    int nLiners = 0;

    Mat img_matches;
    Mat logo_desc = Mat( logo.num_kps, 32, CV_8UC1, logo.desc_add );
    double ransacReprojThreshold = 5.0;
    Mat status;
    uchar val = 0;
#ifdef DEBUG_LD
	vector<Point2f> scene_corners(4);
    Mat img_object = logo.image;
    Mat img_scene = frame.image;
#endif

    p1.reserve(100);
    p2.reserve(100);
    goodmatches.reserve(100);
    matches.reserve(500);
    try
    { 
        matcher.knnMatch(logo_desc, frame.descriptor, matches, 2);
        goodmatches = filterMatches( matches, 
                                     logo.kp_add,
                                     frame.keypoints,
                                     (float)0.85,
                                     p1, 
                                     p2);
    }
    catch( cv::Exception& e )
    {
        const char* err_msg = e.what();
        debug_printf( E_DEBUG_LEVEL_CRITICAL_ERROR, 
                      "exception caught: %s",err_msg );
    }

    if( p1.size() > 4 )
    {
        try
        {         
            Mat H = findHomography( p1, p2, CV_RANSAC, ransacReprojThreshold, 
                                    status );
            nLiners = 0;
            
            for( int i = 0; i < status.rows; i++ )
            {
                uchar* Mi = status.ptr<uchar>(i);
                val = Mi[0];
                if(val == 1)
                    nLiners++;
            }
#ifdef DEBUG_LD
            obj_corners[0] = cvPoint(0,0); 
            obj_corners[1] = cvPoint( logo.image.cols, 0 );
            obj_corners[2] = cvPoint( logo.image.cols, logo.image.rows ); 
            obj_corners[3] = cvPoint( 0, logo.image.rows );
            
            perspectiveTransform( obj_corners, scene_corners, H);
            drawMatches( img_object, logo.keypoints, img_scene, frame.keypoints, 
                     goodmatches, img_matches, Scalar::all(-1), Scalar::all(-1),
                     status, 0/* DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS*/ );
            line( img_matches, scene_corners[0] + Point2f( img_object.cols, 0), 
                  scene_corners[1] + Point2f( img_object.cols, 0), 
                  Scalar(0, 255, 0), 
                  4 );
            line( img_matches, scene_corners[1] + Point2f( img_object.cols, 0), 
                  scene_corners[2] + Point2f( img_object.cols, 0), 
                  Scalar( 0, 255, 0), 
                  4 );
            line( img_matches, scene_corners[2] + Point2f( img_object.cols, 0), 
                  scene_corners[3] + Point2f( img_object.cols, 0), 
                  Scalar( 0, 255, 0), 
                  4 );
            line( img_matches, scene_corners[3] + Point2f( img_object.cols, 0), 
                  scene_corners[0] + Point2f( img_object.cols, 0), 
                  Scalar( 0, 255, 0), 
                  4 );
#endif
        }
        catch( cv::Exception& e )
        {
            const char* err_msg = e.what();
            debug_printf( E_DEBUG_LEVEL_CRITICAL_ERROR, 
                          "exception caught: %s",err_msg );
        }
    }  
#ifdef DEBUG_LD
    if( nLiners > 20 )
	{
        imshow("MyVideo",img_matches);
	}
#endif
    return nLiners;
}

/*
   Filter the found filter matches.
   1. Get the good matchesbased on the distance.
   2. If a single keypoint on the frame is matching multiple keypoints on the 
      template, ignore them.
*/
vector<DMatch> filterMatches( vector<vector<DMatch> >& matches,
                              Point* kp1,
                              vector<KeyPoint>& kp2, 
                              float ratio, 
                              vector<Point2f>& p1, 
                              vector<Point2f>& p2)
{
    vector<DMatch> good_matches;
    int found = 0;
    int s1 = (int)matches.size();

    for( int i = 0; i < (int)matches.size(); i++ )
    {
        vector<DMatch> tMatches = matches[i];
        found = 0;
        if( ( (int)tMatches.size()==2 ) && 
            ( tMatches[0].distance <  (tMatches[1].distance * ratio) ) )
        {
            DMatch m = tMatches[0];            
            Point Y = kp1[m.queryIdx];
            Point X = kp2[m.trainIdx].pt;
            if( p2.size() == 0 )
			{
				p1.push_back(Y);
				p2.push_back(X);
				good_matches.push_back(m);
			}
			else
			{
				int seen_at = 0;
				found = 0;
				for( int j = 0; j < (int) p2.size(); j++ )
				{                    
					Point P = p2[j];
					if( (P.x == X.x) && (P.y == X.y) )
					{
						found++;
					}
					seen_at++;
				}
				if ( found == 0 ) 
				{
					p1.push_back(Y);
					p2.push_back(X);
					good_matches.push_back(m);                
				}
				else
				{
					found = 0;
				}
			}
        }
    }
    return good_matches;
}


