/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2013 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
* File name   :     FingerPrinting.cpp
*
* Author      :     Siddharth Kumar
*
* Description :     Routines for calculating and identifying images using 
                    fingerprinting with perceptual hashes.
*
*******************************************************************************/


/******************************* I N C L U D E S ******************************/
#include "FingerPrinting.h"
#include "VideoDB.h"
#include <opencv2/highgui/highgui.hpp>
#ifdef WIN32
#include "Windows.h"
#include <io.h>
#else
#include <dirent.h>
#endif
#include <iostream>
#include <string>
#include <vector>

using namespace std;

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/
#define EPS                 (1E-4)
#define MAX_HASH_ENTRIES    (5000)
#define MAX_STRING_SIZE     (31)
#define HASH_LENGTH         (64)
#define DCT_THRESHOLD       (0)

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/
typedef unsigned long long int ulong64;

/*********************** L O C A L   S T R U C T U R E S **********************/
struct devFingerprintStruct
{
    int deviceIndex;
	int versionindex;
    int numHashValues;
    ulong64 hashValues[MAX_HASH_ENTRIES];
    char devTitle[MAX_STRING_SIZE];
} ;

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/
//static cv::Mat k      = 1.f/9.f * (cv::Mat_<float>(3,3) << 1, 1, 1, 1, 1, 1, 1, 1, 1);
static int numFPFiles = 0;
static vector<devFingerprintStruct> vDevFPStruct;
#ifdef FP_DEBUG
static FILE *log_file;
string log_dir;
string log_file_name;
string test_image_dump;
#endif

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/
static int PerceptualHash(cv::Mat image, ulong64 *hash);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/

/*----------------------------------------------------------------------------*/

int Fingerprinting(cv::Mat image, vidDetectPayload *sVidDetectPayload)
{
    ulong64 testHashVal = 0;
    bool match = false;
    int device_id = -1;
    int ret = -1;

    PerceptualHash(image, &testHashVal);
    //printf("\n calculated hash: %lld",testHashVal);
    //send the hash value to the processed db for matching
    ret = get_db_data( eFINGERPRINTING, &device_id, (void*) &testHashVal);
    if( match )
    {
        return (0);
    }
    else
    {
        return (-1);
    }
}

/********** S T A T I C   F U N C T I O N   D E F I N I T I O N S *************/
int PerceptualHash(cv::Mat image, ulong64 *hash) 
{
    cv::Mat temp1, temp12, temp2;
    if (image.channels() == 1) {
        temp1 = image.clone();
    } else {
        cv::cvtColor(image, temp1, CV_BGR2GRAY);
    }
    //cv::filter2D(temp1, temp1, CV_32FC1, k);
	//cv::blur( temp1, temp12, cv::Size( 3, 3 ), cv::Point(-1,-1) );
	//cv::bilateralFilter(temp1, temp12, 5, 50, 50 );
	temp1.convertTo(temp1, CV_32FC1);

    temp2 = ResizeImage( temp1, 160, 90, __FUNCTION__ );
    cv::dct(temp2, temp2);

    float valArray[64] = {0,};
    float mean = 0;
    for(int r = 0; r < 8; r++)
    {
        for(int c = 0; c < 8; c++)
        {
            valArray[r*8 + c] = temp2.at<float>(r, c);
            if(fabs(valArray[r*8 + c]) < EPS)
            {
                /* Too low values of DCT cause unnecessary variations in hash.
                For e.g. e-6,-2e-7(in a monochrome image) cause bit changes 
                in hash which may result in false matches */
                valArray[r*8 + c] = 0;
            }
            mean += valArray[r*8+c];
        }
    }
    mean = mean - valArray[0];
    mean = mean/63;

    ulong64 one = 0x0000000000000001;
    *hash = 0x0000000000000000;
    for (int i=0;i< 64;i++){
        float current = valArray[i];
        if (current >= mean)
            *hash |= one;
        one = one << 1;
    }
    return 0;
}

/*----------------------------------EOF---------------------------------------*/
