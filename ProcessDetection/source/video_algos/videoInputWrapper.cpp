/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : videoInputWrapper.h
Author       : Siddharth Kumar
Description  : This file contains C++ interface definition to video detection 
               code that needs to be called from C.
*******************************************************************************/

/******************************* I N C L U D E S ******************************/

#include "videoInputWrapper.h"
#include <sys/time.h>
//#include <tchar.h>
#include <pthread.h>
#include <unistd.h>		/*used for sleep() */
#include <stdlib.h>
#include "branch_utils.h"
#include "branch_pvt_messages.h"
#include "common_header.h"
#include "FingerPrinting.h"
#include "LogoDetection.h"
#include "TemplateMatching.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/
#define CHECK_TIME
/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

typedef enum eSCHEME_INDEX
{
	eIND_FINGERPRINTING    = 0,
	eIND_LOGO_DETECTION    = 1,
	eIND_TEMPLATE_MATCHING = 2,
};

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

//static videoInput * pVideoInputObject = NULL;
static vf_params *pDataQParams;
static vf_params *pDataQParams2;
static vf_params *pDataQParams3;
static vidDetectPayload sVidDetectPayload[NUM_SCHEMES];
static int event_sent[NUM_SCHEMES] = {0,};
timeval start_f, end_f, start_tm, end_tm, start_ld, end_ld;
double time_elapsed_tm = 0, time_elapsed_ld = 0,time_elapsed_f = 0;

/********* S T A T I C   F U N C T I O N   D E C L A R A T I O N S ************/


#ifdef FILE_BASED_DETECTION
static void* read_frame_from_file(void* lpParam);
#endif

static int8_t 
send_video_detected_internal_event (vidDetectPayload sVidDetectPayload);

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/
#ifdef FILE_BASED_DETECTION
vector<videoDB> detect_files_db;
static int num_test_images = 0;
void init_files()
{
	int dBlen = 0;
	dBlen = AccessVideoDB (detect_files_db, "./data_images", "jpg");
	num_test_images = dBlen;
}
#endif
/*int videoDetectionInit(unsigned char * dispFrame)
{
    bool retVal = false;
	int fnRetVal = 0;
	int h_thread		= 0;
    pthread_t	dwThreadId		= 0x00;

     Initialize file based detection
	#ifdef FILE_BASED_DETECTION
    init_files();
    h_thread = pthread_create (
    			&dwThreadId,
                NULL,                     default security attributes
                read_frame_from_file,   thread function name
                0                    use default creation flags
                );             returns the thread identifier
	if( h_thread )
	{
		fnRetVal = -2;
		goto LABEL_RETURN;
	}
	#endif

	 Initialize Detection algorithms
	fnRetVal = initFingerprinting();
	initTemplateMatching();
	initLogoDetection();

	if(fnRetVal < 0)
	{
		debug_printf(E_DEBUG_LEVEL_CRITICAL_ERROR,"Algorithm(s) init unsuccessful");
		goto LABEL_RETURN;
	}
	
	h_thread = pthread_create (
			&dwThreadId,
            NULL,                     default security attributes
            algo_process1_thread,   thread function name
            0                    use default creation flags
            );             returns the thread identifier
    if( h_thread )
    {
        fnRetVal = -2;
        goto LABEL_RETURN;
    }

	h_thread = pthread_create (
			&dwThreadId,                     default security attributes
			NULL,                        use default stack size
            algo_process2_thread,   thread function name
            0);             returns the thread identifier
    if( h_thread )
    {
        fnRetVal = -2;
        goto LABEL_RETURN;
    }

	h_thread = pthread_create (
			&dwThreadId,
            NULL,
            algo_process3_thread,   thread function name
            0 );             returns the thread identifier
    if( h_thread )
    {
        fnRetVal = -2;
        goto LABEL_RETURN;
    }

    memset(sVidDetectPayload, 0, sizeof(sVidDetectPayload));

	sVidDetectPayload[eIND_FINGERPRINTING].sDetectInfo.detScheme = eFINGERPRINTING;
	sVidDetectPayload[eIND_LOGO_DETECTION].sDetectInfo.detScheme = eLOGO_DETECTION;
	sVidDetectPayload[eIND_TEMPLATE_MATCHING].sDetectInfo.detScheme = eTEMPLATE_MATCHING;

	
LABEL_RETURN:
    return fnRetVal;
    
}*/

void videoDetectionRestartDevice()
{
    bool ret = false;
    //ret = pVideoInputObject->restartDevice(0);
    if( !ret )
    {
        debug_printf( E_DEBUG_LEVEL_ERROR, "Failed to restart device\n");
    }
}

void videoDetectionProcess(int *w, int *h)
{
    //*w = pVideoInputObject->getWidth(0);
    //*h = pVideoInputObject->getHeight(0);
}

void videoDetectionDeinit()
{
	//deinitFingerprinting();
    //delete pVideoInputObject;
}

int reset_event_status ()
{
    memset(event_sent, 0, sizeof(event_sent));
    return 0;
}
#ifdef FILE_BASED_DETECTION
static int img_cnt = 0;

static void* read_frame_from_file(void* lpParam)
{
	unsigned char *pixels = NULL;
	string filename;
	Mat frame;
	vf_params dataQParams;
	int frame_size = 0;
	int ret = 0;
	int max_size = 1920*1080*3;

	pixels = (unsigned char*)malloc(max_size);
	while(1)
	{
		//unsigned char *pixels = NULL;
		filename = detect_files_db[img_cnt].dB_filename;
		printf("\nRead file: %s", filename.c_str());
		frame = imread(filename, CV_LOAD_IMAGE_COLOR);
		if(!frame.data)
		{
			debug_printf(E_DEBUG_LEVEL_ERROR, "failed to open image file!");
		}
		else
		{
			dataQParams.height = frame.rows;
			dataQParams.width  = frame.cols;
			dataQParams.planes = 3;
			frame_size = frame.rows*frame.cols*3; /*size in bytes*/
			memset(pixels, 0, max_size);
			memcpy(pixels, frame.data, frame_size);
			//pixels = frame.data;
			if((0 != dataQParams.height) && (0 != dataQParams.width))
			{
				//frameCount++;
				ret = br_send_msg(E_QUEUE_VIDEO_DETECT_DATA, pixels, &dataQParams, 0);
				//if( frameCount/7 > 0 )
				{
					br_send_msg(E_QUEUE_VIDEO_DETECT_DATA2, pixels, &dataQParams, 0);
					br_send_msg(E_QUEUE_VIDEO_DETECT_DATA3, pixels, &dataQParams, 0);
					//frameCount= 0;
				}
			}
			else
			{
				debug_printf(E_DEBUG_LEVEL_DEBUG,"SampleGrabber Callback: Size zero data");
			}
		}
		img_cnt++;
		if(img_cnt > num_test_images)
		{
			img_cnt = 0;
		}
		sleep(10);
	}

}
#endif
void* algo_process1_thread(void* lpParam)
{
	cv::Mat image;
	cv::Mat resizedImg;
	int retVal                   = -1;
	int height                   = 0;
	int width                    = 0;
	int planes                   = 0;
	int invalidImage             = 0;
	unsigned char * src          = NULL;
	unsigned char *dispFrame	 = (unsigned char *)lpParam;
	static int numFrames         = 0;
	static double minMeasure     = 64;
	static int devID             = 0;
	static int devID_prev		 = 0;
	static int verID             = 0;
	static int processComplete   = 0;

	static int num = 0;
	char test_file[50];

	while(1)
	{
		memset(&sVidDetectPayload[eIND_FINGERPRINTING], 0, sizeof(vidDetectPayload));
		sVidDetectPayload[eIND_FINGERPRINTING].sDetectInfo.detScheme = eFINGERPRINTING;

		/*retVal = br_recv_msg(E_QUEUE_VIDEO_DETECT_DATA, (void **)&src, (void **)&pDataQParams, 0);
		if( retVal < 0 )
		{
			//memset(dispFrame, 0, (1920*1080*3));
			sleep(1);
			continue;
		}*/
		sprintf( test_file, "APPLE_TV_VGA_%d.jpg", num);
		num++;
		image = imread(test_file);
		height = image.rows;
		width  = image.cols;
		planes = 3;

		//height= pDataQParams->height;
		//width = pDataQParams->width;
		//planes = pDataQParams->planes;

		//image = Mat(height, width, CV_8UC3, src);

		if( (height != VIDEO_DETECT_FRAME_HEIGHT) && 
                                           (width != VIDEO_DETECT_FRAME_WIDTH) )
		{
			resizedImg = ResizeImage( image, 
                                      VIDEO_DETECT_FRAME_WIDTH, 
                                      VIDEO_DETECT_FRAME_HEIGHT,
                                      __FUNCTION__);
		}
		else
		{
			resizedImg = image;
		}
		
		invalidImage = isSingleColorImage( resizedImg,  99.5);
		if(invalidImage)
		{
			br_free_msg (E_QUEUE_VIDEO_DETECT_DATA);
			sleep(1);
			continue;
		}
#ifdef CHECK_TIME
		gettimeofday(&start_f,NULL);
        //start_f = clock();
#endif
		retVal = Fingerprinting(resizedImg, &sVidDetectPayload[eIND_FINGERPRINTING]);
#ifdef CHECK_TIME
		gettimeofday(&end_f,NULL);
		time_elapsed_f = (end_f.tv_sec+ (double)end_f.tv_usec/1000000) - (start_f.tv_sec+ (double)start_f.tv_usec/1000000);
        //time_elapsed_f =((double) (end_f.tv_usec - start_f.tv_usec)/1000);
                		//printf("\n Time taken to Fingerprinting:%f s", time_elapsed_f);
#endif
		br_free_msg (E_QUEUE_VIDEO_DETECT_DATA);

		if(processComplete == 1)
		{
            if((devID > NO_DEVICE) && (0 == event_sent[eIND_FINGERPRINTING]) /*&& (devID != devID_prev)*/)
			{
				debug_printf(E_DEBUG_LEVEL_INFO,"Fingerprinting %s_%s", DEVICE_NAMES[devID],DEVICE_VER[verID]);
				/* Send VIDEO_DETECTED to video detect thread */
				send_video_detected_internal_event(sVidDetectPayload[eIND_FINGERPRINTING]);
				event_sent[eIND_FINGERPRINTING] = 1;
				devID_prev = devID;
			}
			numFrames = 0;
			minMeasure = 64;
			devID = 0;
			verID = 0;
			processComplete = 0;
		}
		sleep(1);
	}
}

void* algo_process2_thread(void* lpParam)
{
	cv::Mat image;
	cv::Mat resizedImg;
	cv::Mat gImage;
	int retVal          = -1;
	int height          = 0;
	int width           = 0;
    unsigned char * src = NULL;
	int logoDetect_prev = 0;
	static int num = 28;
	char test_file[40];

	while( 1 )
	{
		memset(&sVidDetectPayload[eIND_LOGO_DETECTION], 0, sizeof(vidDetectPayload));
		sVidDetectPayload[eIND_LOGO_DETECTION].sDetectInfo.detScheme = eLOGO_DETECTION;
		/*retVal = br_recv_msg (E_QUEUE_VIDEO_DETECT_DATA2,
                              (void **)&src, (void **)&pDataQParams2, 0);
		if( retVal < 0 )
		{
			 No data to process
    		sleep(1);
			continue;
	    }*/
		sprintf( test_file, "frame_test_%d.JPG", num);
		num++;
		image = imread(test_file);
		height = image.rows;
		width  = image.cols;
        /*height= pDataQParams2->height;
		width = pDataQParams2->width;*/
		//image = Mat(height, width, CV_8UC3, src);

		/*if( (height != VIDEO_DETECT_FRAME_HEIGHT) &&
                                           (width != VIDEO_DETECT_FRAME_WIDTH) )
		{
            resizedImg = ResizeImage( image, 
                                      VIDEO_DETECT_FRAME_WIDTH, 
                                      VIDEO_DETECT_FRAME_HEIGHT,
                                      __FUNCTION__);
        }
		else
		{
			resizedImg = image;
		}*/
		resizedImg = image;
		//cv::imshow("OpenCV video", resizedImg);

		cvtColor(resizedImg, gImage, CV_BGR2GRAY);
#ifdef CHECK_TIME
		gettimeofday(&start_ld,NULL);
#endif
		int logoDetect = LogoDetection(gImage,&sVidDetectPayload[eIND_LOGO_DETECTION]);
#ifdef CHECK_TIME
		gettimeofday(&end_ld,NULL);
		time_elapsed_ld = (end_ld.tv_sec+ (double)end_ld.tv_usec/1000000) - (start_ld.tv_sec+ (double)start_ld.tv_usec/1000000);
        //time_elapsed_ld =((double) (end_ld.tv_usec - start_ld.tv_usec)/1000);
        printf("\n Time taken to Logo Detection:%f s\n", time_elapsed_ld);
#endif
		br_free_msg (E_QUEUE_VIDEO_DETECT_DATA2);
        
        if( (logoDetect > 0) && (0 == event_sent[eIND_LOGO_DETECTION]) /*&& (logoDetect != logoDetect_prev)*/)
        {
			/* Send VIDEO_DETECTED to video detect thread */
			debug_printf(E_DEBUG_LEVEL_INFO,"LogoDetection %s",DEVICE_NAMES[logoDetect]);
			send_video_detected_internal_event(sVidDetectPayload[eIND_LOGO_DETECTION]);
			event_sent[eIND_LOGO_DETECTION] = 1;
			logoDetect_prev = logoDetect;
		}
		sleep(1);
	}
}

void* algo_process3_thread(void* lpParam)
{
	cv::Mat image;
	cv::Mat resizedImg;
	int retVal          = -1;
	int height          = 0;
	int width           = 0;
	unsigned char * src = NULL;
	int templMatch_prev = 0;

	static int num = 1;
	char test_file[40];

	while(1)
	{
		memset( &sVidDetectPayload[eIND_TEMPLATE_MATCHING],
                 0, 
                 sizeof(vidDetectPayload) );
		sVidDetectPayload[eIND_TEMPLATE_MATCHING].sDetectInfo.detScheme 
                                                           = eTEMPLATE_MATCHING;

		/*retVal = br_recv_msg( E_QUEUE_VIDEO_DETECT_DATA3,
                              (void **)&src, 
                              (void **)&pDataQParams3, 
                              0);
		if( retVal < 0 )
		{
			sleep(1);
			continue;
		}*/
		sprintf( test_file, "frame_test_%d.JPG", num);
		num++;
		image = imread(test_file);
		height = image.rows;
		width  = image.cols;

        /*height= pDataQParams3->height;
		width = pDataQParams3->width;
		image = Mat(height, width, CV_8UC3, src);*/

        /*if( (height != VIDEO_DETECT_FRAME_HEIGHT) &&
                                           (width != VIDEO_DETECT_FRAME_WIDTH) )
		{
            resizedImg = ResizeImage( image, 
                                      VIDEO_DETECT_FRAME_WIDTH, 
                                      VIDEO_DETECT_FRAME_HEIGHT,
                                      __FUNCTION__ );
		}
		else
		{
			resizedImg = image;
		}*/
		resizedImg = image;
        //cv::imshow("OpenCV video", resizedImg);
#ifdef CHECK_TIME
        gettimeofday(&start_tm,NULL);
#endif
		int templMatch = TemplateMatching( resizedImg,
                                    &sVidDetectPayload[eIND_TEMPLATE_MATCHING]);
#ifdef CHECK_TIME
		gettimeofday(&end_tm,NULL);
		time_elapsed_tm = (end_tm.tv_sec+ (double)end_tm.tv_usec/1000000) - (start_tm.tv_sec+ (double)start_tm.tv_usec/1000000);
        //time_elapsed_tm =((double) (end_tm.tv_usec - start_tm.tv_usec)/1000);
                		printf("\n Time taken to Template Match:%f", time_elapsed_tm);
#endif
		br_free_msg (E_QUEUE_VIDEO_DETECT_DATA3);
		
        if( (templMatch > 0) && (0 == event_sent[eIND_TEMPLATE_MATCHING]) 
                                        /*&& (templMatch != templMatch_prev)*/)
		{
			/* Send VIDEO_DETECTED to video detect thread */
			debug_printf(E_DEBUG_LEVEL_INFO,"TemplateMatching %s",DEVICE_NAMES[templMatch]);
			send_video_detected_internal_event( 
                                    sVidDetectPayload[eIND_TEMPLATE_MATCHING] );
			event_sent[eIND_TEMPLATE_MATCHING] = 1;
			//templMatch_prev = templMatch;
		}
		sleep(1);
	}
}


static int8_t 
send_video_detected_internal_event (vidDetectPayload sVidDetectPayload)
{
	x_branch_pvt_message x_br_pvt_msg;

	int        ret_val = -1;

	create_pvt_msg (&x_br_pvt_msg, 
		E_PVT_MSG_TYPE_EVENT,
		E_PVT_EVT_VIDEO_DETECTED, 
		NULL, 
		sizeof(vidDetectPayload), 
		(uint8_t *)(&sVidDetectPayload),
		E_QUEUE_VIDEO_DETECT_MSG,
		E_QUEUE_VIDEO_DETECT_MSG
		);

	x_br_pvt_msg.x_pvt_msg_hdr.msg_checksum =
		calculate_pvt_msg_checksum (&x_br_pvt_msg);

	(void)br_send_msg (x_br_pvt_msg.x_pvt_msg_hdr.e_destination,
					   &x_br_pvt_msg, NULL, 0);

	ret_val = 0;

	return (ret_val);
}
