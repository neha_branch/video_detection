/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : videoInputWrapper.h
Author       : Siddharth Kumar
Description  : This file contains C++ interface to video detection code that 
can be called from C.
*******************************************************************************/

#ifndef _VIDEOINPUT_WRAPPER_H_
#define _VIDEOINPUT_WRAPPER_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    int width;
    int height;
    int planes;
}vf_params;

    int videoDetectionInit(unsigned char* frame);

    void videoDetectionProcess(int *w, int *h);

    void videoDetectionDeinit();

    int reset_event_status ();

    void videoDetectionRestartDevice(); /* Restart the complete filter chain */

    void* algo_process1_thread (void* lpParam); /* Fingerprinting */

    void* algo_process2_thread (void* lpParam); /* Logo detection */

    void* algo_process3_thread (void* lpParam); /* Template matching */

#ifdef __cplusplus
     }
#endif

#endif /* _VIDEOINPUT_WRAPPER_H_ */
