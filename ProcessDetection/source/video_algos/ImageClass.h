/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
* File name   :     ImageClass.h
*
* Author      :     Neha Mittal
*
* Description :     Basic functionality for handling images. Includes for openCV 
*
*******************************************************************************/
#ifndef _IMAGE_CLASS_H
#define _IMAGE_CLASS_H

/******************************* I N C L U D E S ******************************/
#include <stdio.h>
#include <iostream>

#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "common_header.h"

using namespace cv;
using namespace std;

/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/

/****************************** F U N C T I O N S *****************************/

Mat ResizeImage( Mat srcImg, int width, int height, const char* func);
Mat _getRawORBKeypoints( Mat img, int nFeat, vector<KeyPoint>& orbKeypoints, 
                         Mat& orbDescriptors, bool drawKP = false);
int isSingleColorImage( Mat image, float threshold);
Mat PlotSideBySide(Mat img1, Mat img2);
string type2str(int type);

#endif /* _IMAGE_CLASS_H */