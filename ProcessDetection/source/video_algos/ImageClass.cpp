/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2014 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
Name         : ImageClass.cpp
Author       : Neha Mittal
Description  : This file contains function definition for ImageClass.h.
*******************************************************************************/

/******************************* I N C L U D E S ******************************/
#include "ImageClass.h"
#include <math.h>

/******************************** E X T E R N S *******************************/

/************************** L O C A L   D E F I N E S *************************/

/*************************** L O C A L   M A C R O S **************************/

/************************ L O C A L   C O N S T A N T S ***********************/

/********************* L O C A L   E N U M E R A T I O N S ********************/

/*********************** L O C A L   S T R U C T U R E S **********************/

/********************* G L O B A L   V A R I A B L E S ************************/

/********************* S T A T I C   V A R I A B L E S ************************/

/********* P R I V A T E   F U N C T I O N   D E C L A R A T I O N S **********/

/********** P U B L I C   F U N C T I O N   D E F I N I T I O N S *************/
Mat ResizeImage(Mat srcImg, int width, int height, const char* caller_func)
{
    Mat resizedImg;
    if (( srcImg.rows == 0 ) || ( srcImg.cols == 0 ))
	{
		resizedImg = Mat( Size(width,height),CV_8UC3 );
        debug_printf (E_DEBUG_LEVEL_ERROR,
                      "ResizeImge: Incorrect image dimension (%s)",caller_func);
	}
    else if( ( width == 0 ) || ( height == 0 ))
    {
        debug_printf( E_DEBUG_LEVEL_ERROR,
                      "ResizeImge: Incorrect resize params (%s)",caller_func);
        resizedImg = srcImg;
    }
	else
	{
        /* OpenCV function to resize the image */
		resize (srcImg, resizedImg, Size(width,height), 0, 0, INTER_LINEAR);
	}
    return(resizedImg);
}

string type2str(int type) 
{
    string r;

    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);

    switch ( depth )
    {
        case CV_8U:  r = "8U"; break;
        case CV_8S:  r = "8S"; break;
        case CV_16U: r = "16U"; break;
        case CV_16S: r = "16S"; break;
        case CV_32S: r = "32S"; break;
        case CV_32F: r = "32F"; break;
        case CV_64F: r = "64F"; break;
        default:     r = "User"; break;
    }

    r += "C";
    r += (chans+'0');

    return r;
}

Mat PlotSideBySide(Mat img1, Mat img2)
{
    Size size1 = img1.size();
    Size size2 = img2.size();
    Mat image(size1.height, 2*size1.width, CV_32FC1); // Copy constructor
    Mat left(image, Rect(0,0,size1.width, size1.height));
    img1.copyTo(left);
    
    Mat right(image, Rect(size1.width, 0, size2.width, size2.height)); // Copy constructor
    img2.copyTo(right);
    return(image);
   /* Mat combine = Mat::zeros(img_object.rows,img_object.cols *2,img_object.type());
for (int i=0;i<combine.cols;i++) {
    if (i < img_object.cols) {
         combine.col(i) = img_object.col(i);
    } else {
         combine.col(i) = img_scene.col(i-img_object.col);
    }*/
}

int isSingleColorImage( Mat image, float threshold)
{    
    Mat hsv;
    if( image.channels() > 1)
    {
        cvtColor(image, hsv, CV_BGR2GRAY);//image;
    }
    else
    {
        hsv = image;
    }

    int /*hbins = 0,*/ sbins = 16;
    int histSize[] = {sbins};

    //float hranges[] = { 0, 1 };

    float sranges[] = { 0, 256 };
    const float* ranges[] = { sranges };
    MatND hist(sbins,1,CV_8U);

    int channels[] = {0};

    calcHist( &hsv, 1, channels, Mat(), // do not use mask
             hist, 1, histSize, ranges,
             true, // the histogram is uniform
             false );

    double maxVal;
    minMaxLoc(hist, 0, &maxVal, 0, 0);
    int size = image.rows*image.cols;
    double a = maxVal*100/size;
    if( a >= threshold )
    {
        return(1);
    }
    else
    {
        return(0);
    }
}

Mat _getRawORBKeypoints(Mat img, int nFeat, vector<KeyPoint>& orbKeypoints, Mat& orbDescriptors, bool drawKP)
{
    //ORB(nlevels = 20, WTA_K = 3,edgeThreshold = 10, patchSize=10) #nfeatures=30,WTA_K = 4,
    Mat outImage;
    ORB orb(nFeat, 1.2f, 20, 10, 0, 3, ORB::HARRIS_SCORE, 10 );

    orb(img, Mat(), orbKeypoints, orbDescriptors, false);

    if( drawKP )
    {
        drawKeypoints(img, orbKeypoints, outImage);
        imshow("MyVideo", ResizeImage(outImage,640,360, __FUNCTION__));
    }
    return (outImage);
}
