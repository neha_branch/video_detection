/***************************** C O P Y R I G H T *******************************
* Copyright (C) 2013 SNAP Networks Inc. & SNAP Networks Pvt. Ltd.              *
*               All Rights Reserved.                                           *
*******************************************************************************/

/************************** F I L E   D E T A I L S ****************************
* File name   :     FingerPrinting.h
*
* Author      :     Siddharth Kumar
*
* Description :     Header file with interfaces fingerprinting routines.
*
*******************************************************************************/
#ifndef _FINGERPRINTING_H_
#define _FINGERPRINTING_H_

/******************************* I N C L U D E S ******************************/
#include <opencv2/core/core.hpp>
#include "common_header.h"
#include "ImageClass.h"
/******************************** E X T E R N S *******************************/

/******************************** D E F I N E S *******************************/

/********************************* M A C R O S ********************************/

/****************************** C O N S T A N T S *****************************/

/*************************** E N U M E R A T I O N S **************************/

/***************************** S T R U C T U R E S ****************************/

/****************************** F U N C T I O N S *****************************/
int initFingerprinting();
int Fingerprinting(cv::Mat image, vidDetectPayload *sVidDetectPayload);
void deinitFingerprinting();

#endif /* _FINGERPRINTING_H_ */
